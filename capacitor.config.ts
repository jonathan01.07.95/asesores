import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'asesores.becrux.com',
  appName: 'asesores',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
