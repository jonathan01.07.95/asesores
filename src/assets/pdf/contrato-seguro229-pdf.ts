/*import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import {IMAGEN_SEGURO} from "../../core/constantes/imagenes-base64.constantes";
import * as moment from "moment";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

export function crearContrato229(
  informacion?: any,
): any {
  const fecha = moment().format('DD-MM-YYYY');
  const imagenSeguro = IMAGEN_SEGURO;

  const pdfDefinicion = {
    content: [
      {
        stack: [
          'AUTORIZACIÓN DE DÉBITO\n',
        ],
        style: 'header',
        alignment: 'center'
      },
      {
        stack: [
          {
            text: [
              'Yo, ', {text: informacion.nombres, style: 'textoNegrilla'}, ' identificado con la cédula de ciudadanía o pasaporte número: ', {text: informacion.identificacion, style: 'textoNegrilla'}, ' autorizo a la', {text: ' COOPERATIVA DE AHORRO Y CRÉDITO 29 DE OCTUBRE LTDA.', style: 'textoNegrilla'}, ' debitar de acuerdo con el período de pago elegido, de mí cuenta: ', {text: informacion.numeroCuenta, style: 'textoNegrilla'}, '\n\n'
            ],

          }
        ],
        style: 'normal'
      },
      {
        columns: [
          {
            width: 140,
            text: 'Cta. Ahorros Socio (X)',
            style: 'normal'
          },
          {
            width: 140,
            text: 'Cta. Ahorros Cliente (  )',
            style: 'normal'
          },
          {
            width: '*',
            text: '\n\n'
          }
        ]
      },
      {
        stack: [
          {
            text: [
              'Entidad Financiera: COOPERATIVA DE AHORRO Y CRÉDITO 29 DE OCTUBRE LTDA\n\n',
            ],
            style: 'normal'
          }
        ],
      },
      {
        stack: [
          {
            text: [
              'Mensual (X) \n\n',
            ],
            style: 'normal'
          }
        ],
      },
      {
        stack: [
          {
            text: [
              'El valor de DOS DÓLARES DE LOS ESTADOS UNIDOS DE AMÉRICA CON 29/100 (USD$2.29) correspondiente a la prima total por el PLAN $ 2.29, de acuerdo con el anexo detallado a continuación',
            ],
            style: 'normal'
          }
        ],
      },
      {
        text: '\n'
      },
      {
        stack: [
          'ANEXO COBERTURAS PLAN $2.29',
        ],
        style: 'tablaTitulo',
        alignment: 'center'
      },
      {
        style: 'tableExample',
        table: {
          headerRows: 1,
          body: [
            [
              {text: '', style: 'tableHeader'},
              {
                image: 'building',
                width: 380,
                height: 450
              },
              {text: '', style: 'tableHeader'}]
          ]
        },
        layout: 'noBorders'
      },
      {
        text: '\n'
      },
      {
        stack: [
          {
            text: [
              'Me comprometo en tal razón, a mantener en mi cuenta el valor autorizado a debitar en la forma y periodicidad indicada. La presente autorización de débito se mantendrá vigente, salvo que personalmente la deje sin efecto',
            ],
            style: 'normal'
          }
        ],
      },
      {
        text: '\n\n\n'
      },
      {
        stack: [
          {
            text: [
              'Firma del Asegurado',
            ],
            style: 'normal'
          }
        ],
      },
      {
        stack: [
          {
            text: [
              `Fecha de autorización: ${fecha}`,
            ],
            style: 'normal'
          }
        ],
      },
    ],
    styles: {
      header: {
        fontSize: 14,
        bold: true,
      },
      textoNegrilla: {
        fontSize: 10,
        bold: true,
      },
      tablaTitulo: {
        fontSize: 12,
        bold: true,
      },
      normal: {
        fontSize: 10,
      },
      tableExample: {
        margin: [50, 2, 0, 0]
      },
    },
    images: {
      building: `data:image/jpeg;base64,${imagenSeguro}`
    }
  }

  return pdfDefinicion;
}

export function descargarContrato229(
  informacion?: any,
): any {
  // pdfMake.vfs = pdfFonts.pdfMake.vfs;
  // pdfMake.createPdf(CrearContrato229(informacion)).download(`CUADRE CAJERO`);
}

export function exportarContrato229(
  informacion?: any,
) {
  const contenido = crearContrato229(informacion);
  const pdfDocGenerator = pdfMake.createPdf(contenido);

  const pdfBlobPromise = new Promise((resolve, reject) => {
    pdfDocGenerator.getBlob((blob: any) => resolve(blob));
  });
  const pdfBase64Promise = new Promise((resolve, reject) => {
    pdfDocGenerator.getBase64((base64: any) => resolve(base64));
  });
  const datosPDFBase64Blob = Promise.all([pdfBlobPromise, pdfBase64Promise]);
  return datosPDFBase64Blob;
}
*/
