export interface ListaInterfacesInterface {
  nombre?: string;
  numeroCuenta?: string
}

export interface RecursosInterface {
  nombre?: string;
}

export interface TiposCapitalizacion {
  nombre?: string;
  pagoInteres?: number;
  periodicidad?: number;
}

export interface CamposModalGeneral {
  titulo: string;
  urlPdf?: string;
  base64?: string;
  cabecera?: string;
  texto?: string;
  urlImagen?: string;
  botonAceptar?: boolean
}

export interface TablaAmortizacionInterface {
  titulo: string;
  info?: any;
}

export interface InformacionSocioInterface {
  esFuncionario: number;
  esVinculado: number;
  esCuentaNueva: number;
  id: number;
  tipoIdentificacion: string;
  identificacion: string;
  codigoDactilar: string;
  nombresCompletos: string;
  nombres: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  email: string;
  telefono: string;
  fechaNacimiento: string;
  nacionalidad: string;
  profesion: string;
  estudios: string;
  estadoCivil: string;
  sexo: string;
  cargas: string;
  edad: string | number;
  ingresoMensual: string | number;
  egresoMensual: string | number;
  patrimonio: string | number;
  dependiente: string;
  tipoVivienda: string;
  tipoSocio: string;
  region: string;
  cuotaEstimada: string | number;
  cuotaAval: string | number;
  montoAval: string | number;
  plazoAval: number | number;
  perfil: string;
  nombreEmpresa: string;
  fechaIngresoEmpresa: string;
  fechaRegistroBase: string;
  direcciones: DireccionSocioInterface[]
}

export interface DireccionSocioInterface {
  id: number;
  pais: string;
  ciudad: string;
  canton: string;
  provincia: string;
  parroquia: string;
  callePrincipal: string;
  calleTransversal: string;
  numeracion: string;
  tipoDireccion: {
    id: number,
    descripcion: string;
  }
}

export interface CuentasSocioInterface {
  cueDescripcion: string;
  cueNumeroCuenta: string;
  cueSaldoTotal: string | number;
  cueSaldoDisponible: string | number;
  cueSaldoBloqueado: string | number;
  cueDescripcionEstado: string;
  cueIdentificacionCliente: string;
  promedioSemestral: string | number;
  cueNumeroCuentaEnmascarada?: string
}

export interface RespuestaInicioSesion {
  usuario: InformacionSocioInterface,
  cuentasSocio: CuentasSocioInterface[],
  token: string
}

export interface TipoCuentaInterface {
  descripcion: string | any;
  estado: number;
  id: number;
  tipoCuenta: string;
}

export interface GeolocalizacionInterface {
  CodigoNegocio: string;
  CodigoPadre: string;
  Descripcion: string;
  IdDetalleCatalogo: string;
  Nivel: string;
}

export interface ProfesionesInterface {
  codigo: string;
  descripcion: string;
}

export interface ActividadesEconomicasInterface {
  CodigoNegocio: string;
  Descripcion: string;
  CodigoPadre?: string;
  IdDetalleCatalogo: string;
  Nivel: string;
}

export interface ValidarInformacionInterface {
  socio: {};
  producto: string;
  simulacion?: {};
  cuentaSeleccionada?: {};
}
