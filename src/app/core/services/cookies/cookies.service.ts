import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {CookieService} from 'ngx-cookie-service';
import {COOKIE_CUENTAS, COOKIE_INFO_SOCIO, COOKIE_OTP, COOKIE_TOKEN} from "../../constantes/nombres-cookies.constantes";

@Injectable({providedIn: 'root'})
export class CookiesService {
  constructor(
    private readonly _cookieService: CookieService,
    private readonly _router: Router,
  ) {
  }

  /** OBTENER COOKIE DATA JSON */
  obtenerCookieJson<Type>(key: string): Type | undefined {
    if (!this._cookieService.get(key)) {
      return undefined
    } else {
      return JSON.parse(this._cookieService.get(key)) as Type;
    }
  }

  /** OBTENER COOKIE DATA STRING */
  obtenerCookie(key: string) {
    return this._cookieService.get(key);
  }

  /** ALMACENAR COOKIE DATA STRING */
  almacenarCookie(key: string, value: string | number | Object) {
    if (typeof value === 'string') {
      this._cookieService.set(key, value, {secure: true});
    } else {
      this._cookieService.set(key, JSON.stringify(value), {secure: true})
    }
  }

  /** ELIMINAR UN VALOR DE COOKIE */
  eliminarCookie(key: string, path?: string) {
    if (path) {
      this._cookieService.delete(key, path);
    }
    this._cookieService.delete(key);
  }

  /** ELIMINAR TODOS LOS VALORES DE COOKIE */
  eliminarTodasCookies(valor?: string) {
    this._cookieService.deleteAll();
    sessionStorage.clear();
    if (valor !== 'inicio') {
      this._router.navigate(['/']).then();
    }
  }

  /** ELIMINAR COOKIE POR EXPIRACION DE TIEMPO */
  eliminarCookieCierreFlujo() {
    this._cookieService.delete(COOKIE_TOKEN, '/');
    this._cookieService.delete(COOKIE_CUENTAS, '/');
    this._cookieService.delete(COOKIE_INFO_SOCIO, '/');
    this._cookieService.delete(COOKIE_OTP, '/');
    this._router.navigate(['./home/apertura-cuenta/ingreso']).then();
  }

  /** ALMACENAR EN SESION STORAGE UN VALOR */
  guardarSesionStorage(nombre: string, valor: any) {
    sessionStorage.setItem(nombre, valor);
  }

  /** OBTENER VALOR DE SESION STORAGE */
  obtenerSessionStorage(nombre: string,) {
    return sessionStorage.getItem(nombre);
  }
}
