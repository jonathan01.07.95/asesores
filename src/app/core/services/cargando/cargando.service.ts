import {Injectable} from '@angular/core';
import {LoadingController} from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class CargandoService implements CargandoServiceInterface {

  constructor(
    private readonly _loadingCtrl: LoadingController
  ) {
  }

  async habilitarCargando(mensaje: string): Promise<void> {
    const loading = await this._loadingCtrl.create({
      message: mensaje,
      cssClass: 'custom-loading',
      keyboardClose: false,
      backdropDismiss: false
    });

    await loading.present()
  }

  async deshabilitarCargando(): Promise<void> {
    await this._loadingCtrl.dismiss()
  }
}

export interface CargandoServiceInterface {
  habilitarCargando(mensaje: string): void;

  deshabilitarCargando(): void;
}

export interface CargandoInterfaceValores {
  bdColor: string,
  size: string,
  color: string,
  type: 'line-spin-clockwise-fade' | 'fire' | 'pacman' | 'ball-scale-multiple';
  fullScreen?: true | false;
  showSpinner?: true | false;
  mensaje: string
}
