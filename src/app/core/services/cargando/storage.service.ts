import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {EncriptadoService} from "../encriptacion/encriptacion-aes.service";
import {CookiesService} from "../cookies/cookies.service";
import {COOKIE_TOKEN} from "../../constantes/nombres-cookies.constantes";

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  /** Obtener datos del usuario quien ingreso */
  private AddLoagueado = new BehaviorSubject<any | undefined>(undefined);
  AddLoagueado$ = this.AddLoagueado.asObservable();

  private AddFormInformacion = new BehaviorSubject<true | false>(false);
  AddFormInformacion$ = this.AddFormInformacion.asObservable();

  private AddFormDireccion = new BehaviorSubject<true | false>(false);
  AddFormDireccion$ = this.AddFormDireccion.asObservable();

  private AddFormFinanciero = new BehaviorSubject<true | false>(false);
  AddFormFinanciero$ = this.AddFormFinanciero.asObservable();

  constructor(
    private readonly _cookiesService: CookiesService,
    private readonly _encriptadoService: EncriptadoService,
  ) {
    const userCurrent = this._cookiesService.obtenerCookie(COOKIE_TOKEN);
    if (userCurrent) {
      const usuarioDesencriptado = JSON.parse(this._encriptadoService.desencriptarInformacionCookie(userCurrent));
      this.setAddLogueado(usuarioDesencriptado!);
    }
  }

  setAddLogueado(data: any | undefined) {
    this.AddLoagueado.next(data);
  }

  setAddForm(data: true | false, formulario: string) {
    switch (formulario) {
      case 'personal':
        this.AddFormInformacion.next(data);
        break;
      case 'direccion':
        this.AddFormDireccion.next(data);
        break;
      case 'financiero':
        this.AddFormFinanciero.next(data);
        break;
    }
  }

}
