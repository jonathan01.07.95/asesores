import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EncriptadoService} from "../encriptacion/encriptacion-aes.service";
import * as moment from "moment";
import {ModalGeneralService} from "../../modales/modal-general.service";
import {HORA_FINAL, HORA_INICIAL} from "../../constantes/valores-constantes";
import {CookiesService} from "../cookies/cookies.service";
import {COOKIE_TOKEN} from "../../constantes/nombres-cookies.constantes";

@Injectable({
  providedIn: 'root'
})
export class RequestInterceptorService implements HttpInterceptor {

  constructor(
    private readonly _encriptadoService: EncriptadoService,
    private readonly _cookiesService: CookiesService,
    private readonly _modalGeneralService: ModalGeneralService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>>| any {
    const horaActual = moment().format('HH:mm:ss');

    if (horaActual > HORA_INICIAL || horaActual < HORA_FINAL) {
      // this._modalGeneralService.mensajeFueraServicio().then();
      // this._cookiesService.deleteAllValue();
    } else {
      request = this.setearToken(request);
      request = this.cifrarInfromacionEnvio(request);
      // request = this.decifrarInformacionRecepcion(request);
      return next.handle(request);
    }
  }

  setearToken(request: any): any {
    const accessTokenString = this._cookiesService.obtenerCookie(COOKIE_TOKEN);

    if (accessTokenString) {
      const accessToken = JSON.parse(this._encriptadoService.desencriptarInformacionCookie(accessTokenString));

      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
      });
      return request;
    } else {
      request = request.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          'XFRS': 'PArra321'
        },
      });
      return request;
    }

  }

  cifrarInfromacionEnvio(request: HttpRequest<any>): any {
    if (request.method === 'POST') {
      const datosEncriptados = this._encriptadoService.encriptarInformacionRequest(JSON.stringify(request.body));
      request = request.clone({
        body: {
          d: datosEncriptados,
        },
      });
      return request;
    }
    return request;
  }

  decifrarInformacionRecepcion(request: HttpRequest<any>): any {
    const datosEncriptados = this._encriptadoService.desencriptarInformacionRequest(JSON.stringify(request.body))
    console.log(datosEncriptados, '==>> desencriptad')
    request = request.clone(datosEncriptados);
    return request;
  }
}
