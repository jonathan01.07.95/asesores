export const TIEMPO_MINUTOS_TOKEN = 3;

export const TIEMPO_SEGUNDOS_TOKEN = 0;

export const VALORES_TIPO_TABLA = [
  {
    nombre: 'CUOTA VARIABLE',
    valor: 'CF'
  },
  {
    nombre: 'CUOTA FIJA',
    valor: 'F'
  }
];

export const VALORES_GENERO = [
  {
    descripcion: 'MASCULINO',
    sexo: 'HOMBRE',
    codigo: 'M',
    codDatabook: 1
  },
  {
    descripcion: 'FEMENINO',
    sexo: 'MUJER',
    codigo: 'F',
    codDatabook: 2
  }
];

export const COLUMNAS_TABLA_AMORTIZACION = [
  {field: 'numeroCuota', header: 'N°.'},
  {field: 'fechaVencimiento', header: 'FECHA\nPAGO'},
  {field: 'capitalProyectado', header: 'CAPITAL'},
  {field: 'interesProyectado', header: 'INTERÉS'},
  {field: 'otrosProyectado', header: 'SEGURO\nPROTECCIÓN'},
  {field: 'seguroProyectado', header: 'SEGURO\nDESGRAVAMEN'},
  {field: 'valor', header: 'CUOTA'},
];

export const POSICIONES_FIRMA_CREDITO = {
  sign_page_number: "0",
  sign_position: "450,100,550,1050"
};

export const POSICIONES_FIRMA_CUENTA_NUEVA = {
  sign_page_number: "10",
  sign_position: "75,120,50,180"
}

export const POSICIONES_SEGURO_FAMILIAR = {
  sign_page_number: "10",
  sign_position: "400,150,550,40"
}

export const HORA_INICIAL = '20:45:00';

export const HORA_FINAL = '03:00:00';

export const DIAS = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

export const ORIGEN_RECURSOS = [
  {
    descripcion: 'Agricultura y Ganadería',
    codigo: 1
  },
  {
    descripcion: 'Elaboración y Fabricación',
    codigo: 2
  },
  {
    descripcion: 'Comercio',
    codigo: 3
  },
  {
    descripcion: 'Servicios en General',
    codigo: 4
  },
  {
    descripcion: 'Empleado Privado',
    codigo: 5
  },
  {
    descripcion: 'Empleado Público',
    codigo: 6
  },
  {
    descripcion: 'Servicios Profesionales',
    codigo: 7
  },
  {
    descripcion: 'Pensión Jubilar',
    codigo: 8
  },
  {
    descripcion: 'Remesas del Exterior',
    codigo: 9
  },
  {
    descripcion: 'Rentas',
    codigo: 10
  },
  {
    descripcion: 'Otros',
    codigo: 11
  },
  {
    descripcion: 'Independiente',
    codigo: 12
  },
]

export const TIPOS_CAPITALIZACION = [
  {
    nombre: 'Mensual',
    pagoInteres: 1,
    periodicidad: 30
  },
  {
    nombre: 'Bimensual',
    pagoInteres: 1,
    periodicidad: 60
  },
  {
    nombre: 'Trimestral',
    pagoInteres: 1,
    periodicidad: 90
  },
  {
    nombre: 'Semestral',
    pagoInteres: 1,
    periodicidad: 180
  },
  {
    nombre: 'Al vencimiento',
    pagoInteres: 0,
    periodicidad: 0
  }
]
