/*import {ValidacionCi} from "../interfaces/validacionCedula.interface";

export const ARREGLO_COEFICIENTES_PERSONA_NATURAL = [2, 1, 2, 1, 2, 1, 2, 1, 2];

export function validarCedula(cedulaT: string) {

  const cedula = cedulaT ? cedulaT.trim() : '';
  const datosValidacioncedula: ValidacionCi = {};
  datosValidacioncedula.numeroCedula = cedula;
  datosValidacioncedula.numeroCaracteresCedula = cedula.length;
  const existeCedula = datosValidacioncedula;
  if (existeCedula) {
    const arregloDigitosCedula = datosValidacioncedula.numeroCedula.split(
      ""
    );
    const dosPrimerosDigitos = +(
      arregloDigitosCedula[0] + arregloDigitosCedula[1]
    );
    const tercerDigito = arregloDigitosCedula[2];
    const esCi = +tercerDigito >= 0 || +tercerDigito <= 5;

    const digitoscedula: string[] = datosValidacioncedula
      .numeroCedula
      .split("")
      .slice(0, 9);
    datosValidacioncedula.digitoVerificador = +arregloDigitosCedula[9];
    datosValidacioncedula.dosPrimerosDigitos = dosPrimerosDigitos;
    datosValidacioncedula.arregloDigitosCedula = digitoscedula;
    if (esCi) {
      datosValidacioncedula.tercerDigito = +tercerDigito;

      return aplicarValidacionesCedula(datosValidacioncedula);
    } else {
      return false;
    }
  } else {
    return false;
  }


  function aplicarValidacionesCedula(parametros: ValidacionCi): boolean {
    const validacionProvincia = validarCodigoProvincia(parametros);
    if (validacionProvincia) {
      const validacionTercerDigito = validarTercerDigito(parametros);
      if (validacionTercerDigito) {

        return algoritmoModulo10(parametros);

      } else {
        console.error("Error tercera validacion!");
        return false;
      }
    } else {
      console.error("No existe provincia!");
      return false;
    }
  }

  // Validación de código de provincia (dos primeros dígitos de CI/RUC)
  function validarCodigoProvincia(parametros: ValidacionCi | any) {
    const valoresEntre0Y24 =
      parametros.dosPrimerosDigitos < 0 || parametros.dosPrimerosDigitos > 24;
    if (valoresEntre0Y24) {
      return false;
    }
    return true;
  }

// Validación de tercer dígito
  function validarTercerDigito(parametros: ValidacionCi) {

    return true;
  }

// validar cedula y ruc persona natural
  function algoritmoModulo10(parametros: ValidacionCi | any): boolean {
    const arregloCoeficientesPersonaNatural = ARREGLO_COEFICIENTES_PERSONA_NATURAL;
    let total: number;
    total = 0;
    let valorPosicion;
    parametros.arregloDigitosCedula.forEach(
      (item: string, indice: number) => {
        let digito: number;
        digito = +item;
        valorPosicion = digito * arregloCoeficientesPersonaNatural[indice];
        if (valorPosicion >= 10) {
          valorPosicion = valorPosicion
            .toString()
            .split("");
          valorPosicion = +valorPosicion[0] + +valorPosicion[1];
        }
        total = total + valorPosicion;
      });
    let residuo: number;
    residuo = total % 10;
    let resultado;
    if (residuo === 0) {
      resultado = 0;
    } else {
      resultado = 10 - residuo;
    }

    if (resultado !== parametros.digitoVerificador) {
      console.error("Dígitos iniciales no validan contra Dígito Idenficador");
      return false;
    }
    return true;
  }
}
*/
