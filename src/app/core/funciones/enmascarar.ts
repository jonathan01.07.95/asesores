export function enmascararFormularioCelular(celular: string, tipoEnmascarar: string) {
  let emascararNombres;
  const tamanio = celular.length - 2;
  const arraynombre1 = celular.split('').slice(2, tamanio);
  const inicio = celular[0] + celular[1];
  emascararNombres = arraynombre1.map((item) => tipoEnmascarar).join('');
  emascararNombres = inicio + emascararNombres + celular[tamanio] + celular[tamanio + 1];
  return emascararNombres;
}

export function enmascararFormulariosCorreo(correo: string, tipoenmascarar: string) {
  let emascararNombres;
  let emascararEmail;
  let correoFinal = '';

  let primeraParte = correo.split('@')[0];
  let segundaParte = correo.split('@')[1].split('.')[0];
  let terceraParte = correo.split('@')[1].split('.')[1];
  let cuartaParte = '';

  if (correo.split('@')[1].split('.')[2]) {
    cuartaParte = `.${correo.split('@')[1].split('.')[2]}`;
  }

  const tamanio = primeraParte.length - 2;
  const arraynombre1 = primeraParte.split('').slice(2, tamanio);
  const inicio = primeraParte[0] + primeraParte[1];

  const tamanio2 = segundaParte.length - 2;
  const arraynombre2 = segundaParte.split('').slice(2, tamanio2);

  emascararNombres = arraynombre1.map((item) => tipoenmascarar).join('');
  emascararNombres = inicio + emascararNombres + primeraParte[tamanio] + primeraParte[tamanio + 1];

  emascararEmail = arraynombre2.map((item) => tipoenmascarar).join('');
  emascararEmail = 'XX' + emascararEmail + segundaParte[tamanio2] + segundaParte[tamanio2 + 1];

  correoFinal = `${emascararNombres}@${emascararEmail}.${terceraParte}${cuartaParte}`;
  return correoFinal;
}

export function enmascararNumeroCuenta(cuenta: string, tipoEnmascarar: string) {
  let emascararCuenta;
  const tamanio = cuenta.length - 2;
  const arraynombre1 = cuenta.split('').slice(2, tamanio);
  const inicio = cuenta[0] + cuenta[1];
  emascararCuenta = arraynombre1.map((item) => tipoEnmascarar).join('');
  emascararCuenta = inicio + emascararCuenta + cuenta[tamanio] + cuenta[tamanio + 1];
  return emascararCuenta;
}
