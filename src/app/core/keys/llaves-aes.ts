import * as crypto from 'crypto-js';

// clave */G4ll3t$**C0m3rc14l2023**_JP --- E6924112DE255698CD85DEB33232DA22E66AEEF8C189EA74D3578489EF55A27F
export const AES_COOKIE = '55D76D0BCF69DA3681F2B4F445853A551B0B99C9948C2477907B771DC1775C19';
const ivCookie = crypto.enc.Utf8.parse(AES_COOKIE);
export const CONFIG_AES_COOKIE = {
  keySize: 256 / 8,
  ivCookie,
  mode: crypto.mode.CBC,
  padding: crypto.pad.Pkcs7
};

// clave */*C0m3rc14l2023**_JP --- 742FEDD6B93D771890289E537CFB796AD2D5A0673A242644A65DD3310655B7FB
export const AES_RUTA = '742FEDD6B93D771890289E537CFB796AD2D5A0673A242644A65DD3310655B7FB';
const iv = crypto.enc.Utf8.parse(AES_RUTA);
export const CONFIG_AES_RUTAS = {
  keySize: 256 / 8,
  iv,
  mode: crypto.mode.CBC,
  padding: crypto.pad.Pkcs7
};

export const AES_REQUEST = 'AD8EEB3030416F9B0730826C7E731C3F5BB22E13DBC1FD52638EABD670A08732';
const ivRequest = crypto.enc.Utf8.parse(AES_REQUEST);
export const CONFIG_AES_REQUEST = {
  keySize: 256 / 8,
  ivRequest,
  mode: crypto.mode.CBC,
  padding: crypto.pad.Pkcs7
};

export const AES_RESPONSE = '5514E242928736F74F9594CF382842FAF166FA767AD35D1D165676F7A664CB25';
const ivResponse = crypto.enc.Utf8.parse(AES_RESPONSE);
export const CONFIG_AES_RESPONSE = {
  keySize: 256 / 8,
  ivResponse,
  mode: crypto.mode.CBC,
  padding: crypto.pad.Pkcs7
};
