import { Injectable } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { ModalMensajeErrorComponent } from '../../shared/modales/modal-mensaje-error/modal-mensaje-error.component';
import {ModalGeneralComponent} from "../../shared/modales/modal-general/modal-general.component";
import {ModalContratoComponent} from "../../shared/modales/modal-contrato/modal-contrato.component";

@Injectable({
  providedIn: 'root',
})
export class ModalGeneralService {
  constructor(
    private readonly _modalController: ModalController,
    private readonly _toastController: ToastController
  ) {}

  async mensajeModalError(mensaje: string, urlImagen: string) {
    const modal = await this._modalController.create({
      component: ModalMensajeErrorComponent,
      componentProps: {
        data: {
          titulo: mensaje,
          urlImagen: urlImagen,
        },
      },
      cssClass: 'mensaje-error',
    });

    await modal.present();
  }

  async mostrarToaster(position: 'top' | 'middle' | 'bottom', mensaje: string) {
    const toast = await this._toastController.create({
      message: mensaje,
      duration: 3500,
      position: position,
      cssClass: 'custom-toast',
    });

    await toast.present();
  }

  async modalGeneral(
    cabecera: string,
    titulo: string,
    texto: string,
    urlImagen: string
  ) {
    const modal = await this._modalController.create({
      component: ModalGeneralComponent,
      componentProps: {
        data: {
          cabecera,
          titulo,
          texto,
          urlImagen,
        },
      },
    });

    await modal.present();
  }

  async modalCambioTab(
    cabecera: string,
    titulo: string,
    texto: string,
    urlImagen: string,
    botonAceptar = false
  ) {
    const modal = await this._modalController.create({
      component: ModalGeneralComponent,
      componentProps: {
        data: {
          cabecera,
          titulo,
          texto,
          urlImagen,
          botonAceptar
        },
      },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();

    if (data == 'confirmar') {
      return true;
    } else {
      return false;
    }
  }

  async modalContratoPdf(
    titulo: string,
    base64: string
  ) {
    try {
      const modal = await this._modalController.create({
        component: ModalContratoComponent,
        componentProps: {
          data: {
            titulo,
            base64,
          },
        },
      });

      await modal.present();
    } catch (e) {

    }
  }
}
