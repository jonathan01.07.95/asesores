import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'apertura-cuenta',
      },
      {
        path: 'apertura-cuenta',
        loadChildren: () => import('../pages/apertura-cuenta/apertura-cuenta.module').then( m => m.AperturaCuentaPageModule),
      },
      {
        path: 'ahorro-programado',
        loadChildren: () => import('../pages/ahorro-programado/ahorro-programado.module').then( m => m.AhorroProgramadoPageModule),
      },
      {
        path: 'plazo-fijo',
        loadChildren: () => import('../pages/plazo-fijo/plazo-fijo.module').then( m => m.PlazoFijoPageModule)
      },
      {
        path: 'credito',
        loadChildren: () => import('../pages/credito/credito.module').then( m => m.CreditoPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
