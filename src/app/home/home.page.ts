import { Component } from '@angular/core';
import { CookiesService } from '../core/services/cookies/cookies.service';
import { ModalGeneralService } from '../core/modales/modal-general.service';
import { ICONO_ALERTA } from '../core/constantes/rutas-constantes';
import { COOKIE_TOKEN } from '../core/constantes/nombres-cookies.constantes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  constructor(
    private readonly _cookiesService: CookiesService,
    private readonly _modalGeneralService: ModalGeneralService,
    private readonly _router: Router
  ) {}

  /*cambioTab(evento: any) {
    console.log(evento, '===>> tab selecionado <<<===');
    const cookieToken = this._cookiesService.obtenerCookie(COOKIE_TOKEN);
    if(cookieToken) {
      // this._modalGeneralService.mensajeModalError('', ICONO_ALERTA)
      this._modalGeneralService
      .modalCambioTab('', 'Estas seguro?', 'Al cambiar de tab deberás iniciar todo el proceso', ICONO_ALERTA, true)
      .then((resp) => {
        console.log(resp, '==>> resp <<==');
        if (resp) {
          const path = `home/${evento.tab}`;
          this._router.navigate([path]).then();
        } else {
          console.log('Se queda aqui mismo');
          
        }
      
      })
    }
    
  } */

}
