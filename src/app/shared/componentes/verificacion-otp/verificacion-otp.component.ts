import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {enmascararFormularioCelular} from "../../../core/funciones/enmascarar";
import * as bcrypt from 'bcryptjs';
import {ModalGeneralService} from "../../../core/modales/modal-general.service";
import {TIEMPO_MINUTOS_TOKEN, TIEMPO_SEGUNDOS_TOKEN} from "../../../core/constantes/valores-constantes";
import {CodeInputComponent} from "angular-code-input";

@Component({
  selector: 'app-verificacion-otp',
  templateUrl: './verificacion-otp.component.html',
  styleUrls: ['./verificacion-otp.component.scss'],
})
export class VerificacionOtpComponent implements OnInit {
  @ViewChild('token') codeInput !: CodeInputComponent;

  minutos: number = TIEMPO_MINUTOS_TOKEN;
  segundos: number = TIEMPO_SEGUNDOS_TOKEN;
  private tiempo: any;
  private date = new Date();
  socio = {
    nombres: 'XXXXX XXXXXX XXXXXXXX XXXX',
    identificacion: '17XXXXXXXX',
    telefono: '0999999999',
  };
  path = '';
  infoToken: any;
  codigoOtp = '';
  codigoErroneo = false;
  habilitar = true;
  celularEnmascarado = '';

  informacionQuery: any = {};

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _modalGeneralService: ModalGeneralService,
  ) {
    this.path = this._router.url.split('/')[2];

    this.celularEnmascarado = enmascararFormularioCelular(this.socio.telefono, 'X');
  }

  ngOnInit() {
    this.empezar();
    this.obtenerTiempoToken();
  }

  /** Verificacion de ingreso codigo */
  digitarCodigo(codigo: string): void {
    this.habilitar = true;
    this.codigoErroneo = false;
  }

  /** Validar codigo completo */
  codigoCompleto(codigo: string): void {
    const validacion = bcrypt.compareSync(codigo, this.infoToken.valorOtp);
    if (validacion) {
      this.codigoOtp = codigo;
      this.codigoErroneo = false;
      this.habilitar = false;
    } else {
      this.codigoErroneo = true;
      this.habilitar = true;
    }
  }

  /** Reenviar token usuario */
  generarNuevoToken(): void {
    this.minutos = TIEMPO_MINUTOS_TOKEN;
    this.segundos = TIEMPO_SEGUNDOS_TOKEN;
    this.empezar();
  }

  /** Funciones manejo de timer */
  decrementar(type: 'M' | 'S'): void {
    if (type === 'M') {
      if (this.minutos <= 0) return;
      this.minutos -= 1;
    } else {
      if (this.segundos <= 0) return;
      this.segundos -= 1;
    }
  }

  /** Decrementar tiempo */
  actualizarTiempo(): void {
    this.date.setMinutes(this.minutos);
    this.date.setSeconds(this.segundos);
    this.date.setMilliseconds(0);

    const time = this.date.getTime();
    this.date.setTime(time - 1000);

    this.minutos = this.date.getMinutes();
    this.segundos = this.date.getSeconds();

    if (this.date.getMinutes() === 0 && this.segundos === 0) {
      clearInterval(this.tiempo);
    }
  }

  /** Empezar tiempo */
  empezar(): void {
    if (this.minutos > 0 || this.segundos > 0) {
      this.actualizarTiempo();
      if (this.segundos > 0) {
        this.tiempo = setInterval(() => {
          this.actualizarTiempo();
        }, 1000)
      } else {
        clearInterval(this.tiempo);
      }
    }
  }

  /** Detener cronometro */
  detener(): void {
    clearInterval(this.tiempo);
  }

  /** Verificar codigo otp */
  verificar(): void {
    let pathRe = '';
    if (this.path === 'apertura-cuenta') {
      pathRe = `./home/${this.path}/tipo-cuenta`;
    } else if (this.path === 'ahorro-programado' || this.path === 'plazo-fijo') {
      pathRe = `./home/${this.path}/seleccion-cuenta-debito`;
    } else {
      pathRe = `./home/${this.path}/resumen`;
    }
    this._router
      .navigate(
        [pathRe])
      .then();
  }

  /** Obtener informacion de tiempo restante */
  obtenerTiempoToken(): void {
    this.minutos = TIEMPO_MINUTOS_TOKEN;
    this.segundos = TIEMPO_SEGUNDOS_TOKEN;
  }
}
