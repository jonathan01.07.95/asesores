import {Component, EventEmitter, Output} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from "@angular/forms";
import {
  ActividadesEconomicasInterface,
  ProfesionesInterface
} from "../../../core/interfaces/lista-interfaces.interface";
import {CookiesService} from "../../../core/services/cookies/cookies.service";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {StorageService} from "../../../core/services/cargando/storage.service";
import * as moment from "moment";

@Component({
  selector: 'app-componente-informacion-personal',
  templateUrl: './componente-informacion-personal.component.html',
  styleUrls: ['./componente-informacion-personal.component.scss'],
})
export class ComponenteInformacionPersonalComponent {
  @Output() formSocio = new EventEmitter<any>()

  formularioInformacion: FormGroup | any;
  infoSocio = {
    "esFuncionario":0,
    "esVinculado":0,
    "esCuentaNueva":0,
    "id":1,
    "tipoIdentificacion":"C",
    "identificacion":"17XXXXXXXX",
    "codigoDactilar":"VXXXXVXXXX",
    "nombresCompletos":"XXXXX XXXXXX XXXXXXX XXXXXX",
    "nombres":"JONATHAN JAVIER",
    "apellidoPaterno":" PARRA",
    "apellidoMaterno":"SOCASI",
    "email":"xxxxxxxx@xxxxxxxx.xxx",
    "telefono":"09XXXXXXXX",
    "fechaNacimiento":"2000-01-01T10:00:00.000Z",
    "nacionalidad":"XXXXXXXXXXX",
    "profesion":"ESTUDIANTE",
    "estudios":"SUPERIOR",
    "estadoCivil":"XXXXXXXX",
    "sexo":"HOMBRE",
    "cargas":0,
    "edad":27,
    "ingresoMensual":1335,
    "egresoMensual":0,
    "patrimonio":10000,
    "dependiente":"Dependiente",
    "tipoVivienda":"Familiar",
    "tipoSocio":"D",
    "region":"PICHINCHA",
    "cuotaEstimada":null,
    "cuotaAval":null,
    "montoAval":null,
    "plazoAval":null,
    "perfil":null,
    "nombreEmpresa":"COOPERATIVA DE AHORRO Y CREDITO 29 DE OCTUBRE LTDA.",
    "fechaIngresoEmpresa":"2021-09-06T10:00:00.000Z",
    "fechaRegistroBase":"2023-05-11T01:58:00.000Z",
    "direcciones":[
      {
        "id":1,
        "pais":"ECUADOR",
        "ciudad":"QUITO",
        "canton":"QUITO",
        "provincia":"PICHINCHA",
        "parroquia":"CONOCOTO",
        "callePrincipal":"ALDAZ 00 CENTRAL",
        "calleTransversal":"SN",
        "numeracion":"SN",
        "tipoDireccion":{
          "id":1,
          "descripcion":"DOMICILIARIA"
        }
      },
      {
        "id":2,
        "pais":"ECUADOR",
        "ciudad":"1701",
        "canton":"1701",
        "provincia":"17",
        "parroquia":"170156",
        "callePrincipal":"CAÑARIS OE6-140 AV. MARISCAL SUCRE DIAGONAL A LA PAPELERIA POPULAR",
        "calleTransversal":"SN",
        "numeracion":"SN",
        "tipoDireccion":{
          "id":2,
          "descripcion":"TRABAJO"
        }
      }
    ],
    "nombresIngresados":"JONATHAN JAVIER",
    "apellidosIngresados":"PARRA SOCASI",
    "login":64
  };

  profesiones: ProfesionesInterface[] = [];
  actividadesEconomicas: ActividadesEconomicasInterface[] = [];

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _cookiesService: CookiesService,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _storageService: StorageService
  ) {

  }

  ngOnInit() {
    if (this.infoSocio.tipoSocio === 'N') {
      this.obtenerActividadesEconomicas();
      this.obtenerProfesiones();
    }
    this.formInicializar();
  }

  obtenerProfesiones(): void {
    this.profesiones = [
      {
        "codigo":"01",
        "descripcion":"ABOGADO (A)"
      },
      {
        "codigo":"112",
        "descripcion":"ARQUEOLOGO"
      },
      {
        "codigo":"04",
        "descripcion":"ARQUITECTO"
      },
      {
        "codigo":"09",
        "descripcion":"CONTADOR PUBLICO AUTORIZADO (TITULADO C.P.A)"
      },
      {
        "codigo":"116",
        "descripcion":"DOCTOR(A) NO MEDICOS"
      },
      {
        "codigo":"11",
        "descripcion":"ECONOMISTAS"
      },
      {
        "codigo":"137",
        "descripcion":"ING. AGRONOMO"
      },
      {
        "codigo":"176",
        "descripcion":"ING. BURSATIL"
      },
      {
        "codigo":"138",
        "descripcion":"ING. CALCULISTA"
      },
      {
        "codigo":"140",
        "descripcion":"ING. COMERCIAL ( ADM. EMPRESAS)"
      },
      {
        "codigo":"141",
        "descripcion":"ING. CONSULTOR"
      },
      {
        "codigo":"142",
        "descripcion":"ING. DE SUELOS"
      },
      {
        "codigo":"143",
        "descripcion":"ING. ELECTRICO"
      },
      {
        "codigo":"144",
        "descripcion":"ING. ELECTRONICO"
      },
      {
        "codigo":"05",
        "descripcion":"ING. EN CONTAB. Y AUDITORIA"
      },
      {
        "codigo":"179",
        "descripcion":"ING. EN ESTADISTICA"
      },
      {
        "codigo":"177",
        "descripcion":"ING. EN FINANZAS"
      },
      {
        "codigo":"175",
        "descripcion":"ING. EN SISTEMAS"
      },
      {
        "codigo":"145",
        "descripcion":"ING. GEOLOGO"
      },
      {
        "codigo":"146",
        "descripcion":"ING. HIDRAULICO"
      },
      {
        "codigo":"174",
        "descripcion":"ING. HOTELERIA Y TURISMO"
      },
      {
        "codigo":"147",
        "descripcion":"ING. INDUSTRIAL"
      },
      {
        "codigo":"178",
        "descripcion":"ING. MARKETING"
      },
      {
        "codigo":"148",
        "descripcion":"ING. MECANICO"
      },
      {
        "codigo":"187",
        "descripcion":"ING. NEGOCIOS INTERNACIONALES"
      },
      {
        "codigo":"149",
        "descripcion":"ING. PETROLERO"
      },
      {
        "codigo":"150",
        "descripcion":"ING. QUIMICO"
      },
      {
        "codigo":"151",
        "descripcion":"ING. SANITARIO"
      },
      {
        "codigo":"191",
        "descripcion":"ING. TELECOMUNICACIONES"
      },
      {
        "codigo":"139",
        "descripcion":"ING.CIVIL"
      },
      {
        "codigo":"17",
        "descripcion":"INGENIEROS (OTROS NO ESPEFICADOS)"
      },
      {
        "codigo":"88",
        "descripcion":"LABORATORISTA"
      },
      {
        "codigo":"182",
        "descripcion":"LCDO. ADM. DE EMPRESAS"
      },
      {
        "codigo":"183",
        "descripcion":"LCDO. ADM. PUBLICA"
      },
      {
        "codigo":"180",
        "descripcion":"LCDO. CONTAB. Y AUDITORIA"
      },
      {
        "codigo":"188",
        "descripcion":"LCDO. EN CIENCIAS DE LA EDUCACION (PROFESOR)"
      },
      {
        "codigo":"186",
        "descripcion":"LCDO. EN PUBLICIDAD"
      },
      {
        "codigo":"15",
        "descripcion":"LCDO. ENFERMERO(A)"
      },
      {
        "codigo":"184",
        "descripcion":"LCDO. MARKETING"
      },
      {
        "codigo":"185",
        "descripcion":"LCDO. OTROS NO ESPECIFICADOS"
      },
      {
        "codigo":"18",
        "descripcion":"LICENCIADO"
      },
      {
        "codigo":"18",
        "descripcion":"LICENCIADOS"
      },
      {
        "codigo":"189",
        "descripcion":"MAESTRIAS, PHD, DIPLOMADOS INTERNACIONALES"
      },
      {
        "codigo":"125",
        "descripcion":"MED. CARDIOLOGO"
      },
      {
        "codigo":"126",
        "descripcion":"MED. CIRUJANO GENERAL"
      },
      {
        "codigo":"127",
        "descripcion":"MED. DERMATOLOGO"
      },
      {
        "codigo":"128",
        "descripcion":"MED. DIABETOLOGO"
      },
      {
        "codigo":"129",
        "descripcion":"MED. ECOGRAFISTA"
      },
      {
        "codigo":"130",
        "descripcion":"MED. ENDOCRINOLOGO"
      },
      {
        "codigo":"131",
        "descripcion":"MED. GASTROENTEROLOGO"
      },
      {
        "codigo":"154",
        "descripcion":"MED. GENERAL"
      },
      {
        "codigo":"132",
        "descripcion":"MED. GERIATRA"
      },
      {
        "codigo":"133",
        "descripcion":"MED. GINECOLOGO"
      },
      {
        "codigo":"134",
        "descripcion":"MED. HEMATOLOGO"
      },
      {
        "codigo":"135",
        "descripcion":"MED. HOMEOPATA"
      },
      {
        "codigo":"136",
        "descripcion":"MED. INFECTOLOGO"
      },
      {
        "codigo":"153",
        "descripcion":"MED. MASTOLOGO"
      },
      {
        "codigo":"155",
        "descripcion":"MED. NEFROLOGO"
      },
      {
        "codigo":"156",
        "descripcion":"MED. NEUROCIRUJANO"
      },
      {
        "codigo":"157",
        "descripcion":"MED. NEUROLOGO"
      },
      {
        "codigo":"158",
        "descripcion":"MED. OFTALMOLOGO"
      },
      {
        "codigo":"159",
        "descripcion":"MED. ORTOPEDISTA"
      },
      {
        "codigo":"160",
        "descripcion":"MED. OTORRINALARINGOLO"
      },
      {
        "codigo":"161",
        "descripcion":"MED. PATOLOGO"
      },
      {
        "codigo":"162",
        "descripcion":"MED. PEDIATRA"
      },
      {
        "codigo":"163",
        "descripcion":"MED. PODOLOGO"
      },
      {
        "codigo":"164",
        "descripcion":"MED. PROCTOLOGO"
      },
      {
        "codigo":"169",
        "descripcion":"MED. QUIROPRACTICO"
      },
      {
        "codigo":"170",
        "descripcion":"MED. RADIOLOGO"
      },
      {
        "codigo":"106",
        "descripcion":"MED. RADIOLOGO"
      },
      {
        "codigo":"171",
        "descripcion":"MED. REUMATOLOGO"
      },
      {
        "codigo":"172",
        "descripcion":"MED. SEXOLOGO"
      },
      {
        "codigo":"173",
        "descripcion":"MED. UROLOGO"
      },
      {
        "codigo":"181",
        "descripcion":"MILITAR"
      },
      {
        "codigo":"121",
        "descripcion":"NINGUNA"
      },
      {
        "codigo":"102",
        "descripcion":"NUTRICIONISTA"
      },
      {
        "codigo":"54",
        "descripcion":"OBSTETRIZ"
      },
      {
        "codigo":"104",
        "descripcion":"PERIODISTA LCDO. EN COMUNICACION SOCIAL"
      },
      {
        "codigo":"56",
        "descripcion":"PSICOLOGO"
      },
      {
        "codigo":"165",
        "descripcion":"PSICOLOGO CLINICO"
      },
      {
        "codigo":"166",
        "descripcion":"PSICOLOGO EDUCATIVO"
      },
      {
        "codigo":"167",
        "descripcion":"PSICOLOGO INFANTIL"
      },
      {
        "codigo":"168",
        "descripcion":"PSICOLOGO ORIENTADOR"
      },
      {
        "codigo":"57",
        "descripcion":"PSICOTERAPEUTA"
      },
      {
        "codigo":"56",
        "descripcion":"SECRETARIA EJECUTIVA"
      },
      {
        "codigo":"35",
        "descripcion":"TECNICO"
      },
      {
        "codigo":"77",
        "descripcion":"TEOLOGO (SACERDOTE)"
      },
      {
        "codigo":"94",
        "descripcion":"TERAPISTA"
      },
      {
        "codigo":"61",
        "descripcion":"TOPOGRAFO"
      },
      {
        "codigo":"117",
        "descripcion":"TRABAJO SOCIAL (LICENCIADOS)"
      },
      {
        "codigo":"62",
        "descripcion":"VETERINARIO"
      }
    ];
  }

  obtenerActividadesEconomicas(): void {
    this.actividadesEconomicas = [
      {
        "IdDetalleCatalogo":"926",
        "CodigoNegocio":"01",
        "Descripcion":"Azuay",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"928",
        "CodigoNegocio":"02",
        "Descripcion":"Bolivar",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"927",
        "CodigoNegocio":"03",
        "Descripcion":"Canar",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"925",
        "CodigoNegocio":"04",
        "Descripcion":"Carchi",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"922",
        "CodigoNegocio":"06",
        "Descripcion":"Chimborazo",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"921",
        "CodigoNegocio":"05",
        "Descripcion":"Cotopaxi",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"914",
        "CodigoNegocio":"07",
        "Descripcion":"El Oro",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"915",
        "CodigoNegocio":"08",
        "Descripcion":"Esmeraldas",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"935",
        "CodigoNegocio":"20",
        "Descripcion":"Galapagos",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"917",
        "CodigoNegocio":"09",
        "Descripcion":"Guayas",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"923",
        "CodigoNegocio":"10",
        "Descripcion":"Imbabura",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"920",
        "CodigoNegocio":"11",
        "Descripcion":"Loja",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"918",
        "CodigoNegocio":"12",
        "Descripcion":"Los Rios",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"916",
        "CodigoNegocio":"13",
        "Descripcion":"Manabi",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"932",
        "CodigoNegocio":"14",
        "Descripcion":"Morona Santiago",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"931",
        "CodigoNegocio":"15",
        "Descripcion":"Napo",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"929",
        "CodigoNegocio":"22",
        "Descripcion":"Orellana",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"933",
        "CodigoNegocio":"16",
        "Descripcion":"Pastaza",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"924",
        "CodigoNegocio":"17",
        "Descripcion":"Pichincha",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"686",
        "CodigoNegocio":"24",
        "Descripcion":"Santa Elena",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"681",
        "CodigoNegocio":"23",
        "Descripcion":"Santo Domingo De Los Tsachilas",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"934",
        "CodigoNegocio":"21",
        "Descripcion":"Sucumbios",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"919",
        "CodigoNegocio":"18",
        "Descripcion":"Tungurahua",
        "Nivel":"3",
        "CodigoPadre":"744"
      },
      {
        "IdDetalleCatalogo":"930",
        "CodigoNegocio":"19",
        "Descripcion":"Zamora Chinchipe",
        "Nivel":"3",
        "CodigoPadre":"744"
      }
    ];

  }

  formInicializar() {
    let fechaNacimientoI = moment(this.infoSocio.fechaNacimiento).format('YYYY-MM-DD')
    this.formularioInformacion = this._formBuilder.group({
      nombresCompletos: new FormControl(this.infoSocio.nombresCompletos),
      fechaNacimiento: new FormControl(fechaNacimientoI),
      genero: new FormControl(this.infoSocio.sexo),
      nacionalidad: new FormControl(this.infoSocio.nacionalidad),
      estadoCivil: new FormControl(this.infoSocio.estadoCivil),
      correo: new FormControl('xxxxxxxxxxxxxxx@xxxxxx.com', [
        Validators.required,
        Validators.email
      ]),
      confirmacionCorreo: new FormControl('xxxxxxxxxxxxxxx@xxxxxx.com', [
        Validators.required,
        Validators.email,
      ]),
      profesion: new FormControl({value: '', disabled: this.infoSocio.tipoSocio === 'D'}, [
        Validators.required,
      ]),
      actividad: new FormControl({value: '', disabled: this.infoSocio.tipoSocio === 'D'}, [
        Validators.required,
      ])
    }, {
      validator: this.validarQueSeanIguales
    })
  }

  get correoField() {
    this.devolverFormulario();
    return this.formularioInformacion.get('correo') as FormControl;
  }

  get confirmacionCorreoField() {
    this.devolverFormulario();
    return this.formularioInformacion.get('confirmacionCorreo') as FormControl;
  }

  get profesionField() {
    this.devolverFormulario();
    return this.formularioInformacion.get('profesion') as FormControl;
  }

  get actividadField() {
    this.devolverFormulario();
    return this.formularioInformacion.get('actividad') as FormControl;
  }

  validarQueSeanIguales: any = (
    control: FormGroup
  ): ValidationErrors | null => {
    const correo = control.get('correo');
    const confirmarCorreo = control.get('confirmacionCorreo');

    if (correo?.value) {
      correo.value.toLowerCase();
    }

    if (confirmarCorreo?.value) {
      confirmarCorreo.value.toLowerCase();
    }

    return correo?.value === confirmarCorreo?.value
      ? null
      : {noSonIguales: true}
  }

  checarSiSonIguales(): boolean {
    return this.formularioInformacion.hasError('noSonIguales') &&
      this.formularioInformacion.get('correo').dirty &&
      this.formularioInformacion.get('confirmacionCorreo').dirty;
  }

  devolverFormulario() {
    if (this.formularioInformacion.valid) {
      this.formSocio.emit(this.formularioInformacion.value);
      this._storageService.setAddForm(false, 'personal');
    } else {
      this._storageService.setAddForm(true, 'personal');
    }
  }
}
