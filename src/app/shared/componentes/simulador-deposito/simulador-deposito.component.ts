import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TiposCapitalizacion} from '../../../core/interfaces/lista-interfaces.interface';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  ORIGEN_RECURSOS,
  TIPOS_CAPITALIZACION,
} from '../../../core/constantes/valores-constantes';
import {CargandoService} from '../../../core/services/cargando/cargando.service';
import {ModalGeneralService} from '../../../core/modales/modal-general.service';
import {ICONO_ERROR} from '../../../core/constantes/rutas-constantes';
import {
  ModalTablaAmortizacionComponent
} from '../../modales/modal-tabla-amortizacion/modal-tabla-amortizacion.component';
import {ModalController} from '@ionic/angular';
import {EncriptadoService} from '../../../core/services/encriptacion/encriptacion-aes.service';
import {CookiesService} from "../../../core/services/cookies/cookies.service";

@Component({
  selector: 'app-simulador-deposito',
  templateUrl: './simulador-deposito.component.html',
  styleUrls: ['./simulador-deposito.component.scss'],
})
export class SimuladorDepositoComponent implements OnInit {
  simuladorDdfForm: FormGroup | any;
  informacionQuery: any = {};

  habilitarContinuar = false;

  path = '';
  montoFinal = 0;
  simulacion: any;

  origenRecursos = ORIGEN_RECURSOS;
  tiposCapitalizacion: TiposCapitalizacion[] = TIPOS_CAPITALIZACION;

  constructor(
    private readonly _router: Router,
    private readonly _formBuilder: FormBuilder,
    private readonly _cargandoService: CargandoService,
    private readonly _modalGeneralService: ModalGeneralService,
    private readonly _modalController: ModalController,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cookiesService: CookiesService
  ) {
    this.path = this._router.url.split('/')[2];
  }

  ngOnInit() {
    this.formInicializar();
  }

  formInicializar() {
    this.simuladorDdfForm = this._formBuilder.group({
      monto: new FormControl(650, [
        Validators.required,
        Validators.min(12),
        Validators.max(50000),
      ]),
      plazo: new FormControl(18, [
        Validators.required,
        Validators.min(6),
        Validators.max(120),
      ]),
      capitalizacion: new FormControl(0, [Validators.required]),
      origenRecursos: new FormControl(1, [Validators.required]),
      interesAdicional: new FormControl(0, [
        Validators.min(0),
        Validators.max(2),
      ]),
    });
  }

  get montoField() {
    return this.simuladorDdfForm.get('monto') as FormControl;
  }

  get plazoField() {
    return this.simuladorDdfForm.get('plazo') as FormControl;
  }

  get tipoCapitalizacionField() {
    return this.simuladorDdfForm.get('capitalizacion') as FormControl;
  }

  get origenRecursosField() {
    return this.simuladorDdfForm.get('origenRecursos') as FormControl;
  }

  get interesAdicionalField() {
    return this.simuladorDdfForm.get('interesAdicional') as FormControl;
  }

  continuar(): void {
    let pathRe;
    if (this.informacionQuery.logueado == true) {
      pathRe = `./home/${this.path}/seleccion-cuenta-debito`;
      const queryJSON = this.informacionQuery;
      this._router
        .navigate([pathRe], {
          queryParams: {
            d: queryJSON,
          },
          // relativeTo: this._activatedRoute,
          queryParamsHandling: 'merge',
        })
        .then();
    } else {
      pathRe = `./home/${this.path}/ingreso`;
      const queryJSON = this.informacionQuery;
      this._router
        .navigate([pathRe], {
          queryParams: {
            d: queryJSON,
          },
          // relativeTo: this._activatedRoute,
          queryParamsHandling: 'merge',
        })
        .then();
    }
  }

  simular(): void {

    const resp = {
      "status": "ok",
      "Impuesto": "0.00",
      "Interes": "74.10",
      "Neto": "74.10",
      "PorcentajeImpuesto": "0.00",
      "TablaPagos": null,
      "TablaSimulaciones": null,
      "TasaEfectiva": "7.46",
      "TasaNominal": "7.6000",
      "montoFinal": 724.1
    }
    this.habilitarContinuar = true;
    this.montoFinal = +(
      600 + Number(resp.Interes)
    ).toFixed(2);
    this.simulacion = resp;
    resp.montoFinal = this.montoFinal;
    this.simulacion = resp;
  }

  async tablaAmortizacion(resp: any): Promise<void> {
    const modal = await this._modalController.create({
      component: ModalTablaAmortizacionComponent,
      componentProps: {
        data: {
          titulo: 'Tabla amortización',
          info: resp.TablaPagos.PagoPeriodico,
        },
      },
    });
    await modal.present();
  }

}
