import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-simulador-credito',
  templateUrl: './simulador-credito.component.html',
  styleUrls: ['./simulador-credito.component.scss'],
})
export class SimuladorCreditoComponent implements OnInit {

  path = '';

  constructor(
    private readonly _router: Router
  ) {
    this.path = this._router.url.split('/')[2];
  }

  ngOnInit() {}

  continuar(): void {
    const pathRe = `./home/${this.path}/ingreso`
    this._router
      .navigate([pathRe])
      .then();
  }
}
