import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {COOKIE_INFO_SOCIO} from "../../../core/constantes/nombres-cookies.constantes";
import {CookiesService} from "../../../core/services/cookies/cookies.service";

@Component({
  selector: 'app-instrucciones-biometrico',
  templateUrl: './instrucciones-biometrico.component.html',
  styleUrls: ['./instrucciones-biometrico.component.scss'],
})
export class InstruccionesBiometricoComponent implements OnInit {

  path = '';
  informacionQuery: any;
  infoSocio: any;

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _cookiesService: CookiesService
  ) {
    this.path = this._router.url.split('/')[2];
  }

  ngOnInit() {
  }

  regresar() {
    let pathRe = '';
    if (this.path == 'apertura-cuenta') {
      pathRe = `./home/${this.path}/tipo-cuenta`;
    } else {
      pathRe = `./home/${this.path}/resumen`;
    }
    this._router
      .navigate([pathRe])
      .then();
  }

  continuar() {
    const pathRe = `./home/${this.path}/informacion-personal`
    this._router
      .navigate([pathRe])
      .then();
  }

}
