import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DIAS} from "../../../core/constantes/valores-constantes";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {CargandoService} from "../../../core/services/cargando/cargando.service";
import {ModalGeneralService} from "../../../core/modales/modal-general.service";

@Component({
  selector: 'app-simulador-ahorro',
  templateUrl: './simulador-ahorro.component.html',
  styleUrls: ['./simulador-ahorro.component.scss'],
})
export class SimuladorAhorroComponent implements OnInit {
  simuladorAPForm: FormGroup | any;
  informacionQuery: any = {};
  habilitarContinuar = false;

  montoFinal = 0;
  interesEfectivo = 0;
  fechaExpiracion = '';

  fechas = DIAS;
  path = '';

  constructor(
    private readonly _router: Router,
    private readonly _formBuilder: FormBuilder,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cargandoService: CargandoService,
    private readonly _modalGeneralService: ModalGeneralService
  ) {
    this.path = this._router.url.split('/')[2];

    this._activatedRoute.queryParams.subscribe((params: any) => {
      if (params.d) {
        this.informacionQuery =
          this._encriptadoService.desencriptarInformacionRutas(params.d);
      }
    });

    console.log(this.informacionQuery, '===>>> informacion del queryy <<===');
  }

  ngOnInit() {
    this.formInicializar();
  }

  formInicializar() {
    this.simuladorAPForm = this._formBuilder.group({
      monto: new FormControl(80, [
        Validators.required,
        Validators.min(12),
        Validators.max(50000),
      ]),
      plazo: new FormControl(12, [
        Validators.required,
        Validators.min(6),
        Validators.max(120),
      ]),
      // fechaInicio: new FormControl(moment().format('YYYY-MM-DD'), [Validators.required]),
      montoInicial: new FormControl(25, [Validators.required]),
      diaDebito: new FormControl(18, [Validators.required]),
    });
  }

  get montoField() {
    return this.simuladorAPForm.get('monto') as FormControl;
  }

  get plazoField() {
    return this.simuladorAPForm.get('plazo') as FormControl;
  }

  get montoInicialField() {
    return this.simuladorAPForm.get('montoInicial') as FormControl;
  }

  get diaDebitoField() {
    return this.simuladorAPForm.get('diaDebito') as FormControl;
  }

  simularAhorroProgramado(): void {
    const respS = {
      status: "ok",
      fechaExpiracion: "2024-06-03T00:00:00",
      interes: "39.91",
      tasaInteres: "7.50",
      totalCuotas: "960"
    }
    this.habilitarContinuar = true;
    this.montoFinal = Number(respS.totalCuotas) + Number(respS.interes);
    this.interesEfectivo = +respS.tasaInteres;
    this.fechaExpiracion = respS.fechaExpiracion.split('T')[0];
  }

  continuar(): void {
    let pathRe;
    if (this.informacionQuery.logueado == true) {
      pathRe = `./home/${this.path}/seleccion-cuenta-debito`;
      this._router
        .navigate([pathRe], {
          queryParams: {
            d: this.informacionQuery,
          },
          queryParamsHandling: 'merge',
        })
        .then();
    } else {
      pathRe = `./home/${this.path}/ingreso`;
      this.informacionQuery.logueado == true
      this._router
        .navigate([pathRe], {
          queryParams: {
            d: this.informacionQuery,
          },
          queryParamsHandling: 'merge',
        })
        .then();
    }
  }
}
