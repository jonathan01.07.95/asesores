import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  AlertController,
  ModalController,
  NavController,
  PopoverController,
} from '@ionic/angular';
import {ModalContratoComponent} from '../../modales/modal-contrato/modal-contrato.component';
import {ModalGeneralService} from '../../../core/modales/modal-general.service';
import {CargandoService} from '../../../core/services/cargando/cargando.service';
import {ICONO_ERROR} from '../../../core/constantes/rutas-constantes';
import {CookiesService} from '../../../core/services/cookies/cookies.service';
import {EncriptadoService} from '../../../core/services/encriptacion/encriptacion-aes.service';
import {
  COOKIE_CUENTAS,
  COOKIE_INFO_SOCIO,
  COOKIE_OTP,
  COOKIE_TOKEN,
} from '../../../core/constantes/nombres-cookies.constantes';
import {StorageService} from "../../../core/services/cargando/storage.service";

@Component({
  selector: 'app-formulario-ingreso',
  templateUrl: './formulario-ingreso.component.html',
  styleUrls: ['./formulario-ingreso.component.scss'],
})
export class FormularioIngresoComponent implements OnInit {
  path = '';
  infoDispositivo: any = {};
  infoGeolocalizacion: any = {};

  formularioIngreso: FormGroup | any;
  cedulaCorrecta = false;

  informacionQuery: any = {};

  constructor(
    private readonly _router: Router,
    private readonly _navController: NavController,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _formBuilder: FormBuilder,
    private readonly _popoverController: PopoverController,
    private readonly _modalController: ModalController,
    private readonly _alertController: AlertController,
  ) {
    this.path = this._router.url.split('/')[2];
  }

  ngOnInit(): void {
    this.formInicializar();
  }

  formInicializar() {
    this.formularioIngreso = this._formBuilder.group({
      apellidos: new FormControl('XXXXXX XXXXXX', [
        Validators.required,
        Validators.minLength(5),
      ]),
      nombres: new FormControl('XXXXXX XXXXXXXX', [
        Validators.required,
        Validators.minLength(5),
      ]),
      identificacion: new FormControl('17XXXXXXXX', [
        Validators.required,
        Validators.minLength(10),
      ]),
      codigoDactilar: new FormControl('VXXXXVXXXX', [
        Validators.required,
        Validators.minLength(10),
      ]),
      celular: new FormControl('0999999999', [
        Validators.required,
        Validators.minLength(10),
      ]),
      /*terminos: new FormControl(false,
        [
          Validators.requiredTrue
        ])*/
    });
  }

  get nombresField() {
    return this.formularioIngreso.get('nombres') as FormControl;
  }

  get apellidosField() {
    return this.formularioIngreso.get('apellidos') as FormControl;
  }

  get identificacionField() {
    return this.formularioIngreso.get('identificacion') as FormControl;
  }

  get codigoDactilarField() {
    return this.formularioIngreso.get('codigoDactilar') as FormControl;
  }

  get celularField() {
    return this.formularioIngreso.get('celular') as FormControl;
  }

  async popDactilar(): Promise<void> {
  }

  async popTerminosCondiciones(): Promise<void> {
    // assets/pdf/AutorizacionConsultaBuro.pdf
  }

  continuar(): void {
    const pathRe = `./home/${this.path}/verificacion-otp`;
    this._router.navigate([pathRe]).then();
  }
}
