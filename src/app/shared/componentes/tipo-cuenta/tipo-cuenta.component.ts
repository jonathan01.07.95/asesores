import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CuentasSocioInterface, TipoCuentaInterface} from "../../../core/interfaces/lista-interfaces.interface";

@Component({
  selector: 'app-tipo-cuenta',
  templateUrl: './tipo-cuenta.component.html',
  styleUrls: ['./tipo-cuenta.component.scss'],
})
export class TipoCuentaComponent implements OnInit {

  path = '';
  cuentasSocio: CuentasSocioInterface[] = [];
  tiposCuentas: TipoCuentaInterface[] = [];
  informacionQuery: any;
  infoSocio = {
    nombres: 'XXXXX XXXXXX XXXXXXXX XXXX',
    identificacion: '17XXXXXXXX',
    telefono: '0999999999',
  };
  cuentasDisponibles: TipoCuentaInterface[] = [];

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
  ) {
    this.path = this._router.url.split('/')[2];
  }

  async ngOnInit(): Promise<void> {
    this.tiposCuentas = [
      {
        estado: 1,
        id: 1,
        tipoCuenta: "AHORROS CLIENTE",
        descripcion: [
          "Disponibilidad inmediata del dinero",
          "Atractiva rentabilidad que puede alcanzar el 3.5%",
          "Acceso a  canales electrónicos"
        ]
      }
    ]
    await this.cuentasDisponiblesCrear();
  }

  tipoCuentaSeleccionada(tipoCuenta: TipoCuentaInterface): void {
    const pathRe = `./home/${this.path}/instruccion-biometrico`;
    this._router
      .navigate([pathRe])
      .then();
  }

  async cuentasDisponiblesCrear() {
    for (const cuenta of this.cuentasSocio) {
      if (cuenta.cueDescripcion.replace(/\s+/g, '') == 'AHORROSSOCIO') {
        const index = this.tiposCuentas.findIndex(obj => obj.tipoCuenta.replace(/\s+/g, '') === 'AHORROSSOCIO');
        if (index !== -1) {
          this.tiposCuentas.splice(index, 1);
        }
      }
    }
  }
}
