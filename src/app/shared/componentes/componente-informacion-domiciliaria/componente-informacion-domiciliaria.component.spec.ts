import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ComponenteInformacionDomiciliariaComponent } from './componente-informacion-domiciliaria.component';

describe('ComponenteInformacionDomiciliariaComponent', () => {
  let component: ComponenteInformacionDomiciliariaComponent;
  let fixture: ComponentFixture<ComponenteInformacionDomiciliariaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponenteInformacionDomiciliariaComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ComponenteInformacionDomiciliariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
