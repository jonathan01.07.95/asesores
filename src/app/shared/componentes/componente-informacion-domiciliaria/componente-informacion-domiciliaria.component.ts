import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GeolocalizacionInterface} from "../../../core/interfaces/lista-interfaces.interface";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CookiesService} from "../../../core/services/cookies/cookies.service";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {StorageService} from "../../../core/services/cargando/storage.service";

@Component({
  selector: 'app-componente-informacion-domiciliaria',
  templateUrl: './componente-informacion-domiciliaria.component.html',
  styleUrls: ['./componente-informacion-domiciliaria.component.scss'],
})
export class ComponenteInformacionDomiciliariaComponent implements OnInit {

  formularioDireccion: FormGroup | any;

  @Output() formDireccion = new EventEmitter<any>()

  listaProvincias: GeolocalizacionInterface[] = [];
  listaCantones: GeolocalizacionInterface[] = [];
  listaParroquias: GeolocalizacionInterface[] = [];

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _cookiesService: CookiesService,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _storageService: StorageService
  ) {}

  ngOnInit() {
    this.obtenerLocalidades('744', '3');
    this.formInicializar();
  }

  obtenerLocalidades(codigoPadre: string, nivel: string): void {
    this.listaProvincias = [
      {
        "IdDetalleCatalogo": "926",
        "CodigoNegocio": "01",
        "Descripcion": "Azuay",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "928",
        "CodigoNegocio": "02",
        "Descripcion": "Bolivar",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "927",
        "CodigoNegocio": "03",
        "Descripcion": "Canar",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "925",
        "CodigoNegocio": "04",
        "Descripcion": "Carchi",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "922",
        "CodigoNegocio": "06",
        "Descripcion": "Chimborazo",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "921",
        "CodigoNegocio": "05",
        "Descripcion": "Cotopaxi",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "914",
        "CodigoNegocio": "07",
        "Descripcion": "El Oro",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "915",
        "CodigoNegocio": "08",
        "Descripcion": "Esmeraldas",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "935",
        "CodigoNegocio": "20",
        "Descripcion": "Galapagos",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "917",
        "CodigoNegocio": "09",
        "Descripcion": "Guayas",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "923",
        "CodigoNegocio": "10",
        "Descripcion": "Imbabura",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "920",
        "CodigoNegocio": "11",
        "Descripcion": "Loja",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "918",
        "CodigoNegocio": "12",
        "Descripcion": "Los Rios",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "916",
        "CodigoNegocio": "13",
        "Descripcion": "Manabi",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "932",
        "CodigoNegocio": "14",
        "Descripcion": "Morona Santiago",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "931",
        "CodigoNegocio": "15",
        "Descripcion": "Napo",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "929",
        "CodigoNegocio": "22",
        "Descripcion": "Orellana",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "933",
        "CodigoNegocio": "16",
        "Descripcion": "Pastaza",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "924",
        "CodigoNegocio": "17",
        "Descripcion": "Pichincha",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "686",
        "CodigoNegocio": "24",
        "Descripcion": "Santa Elena",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "681",
        "CodigoNegocio": "23",
        "Descripcion": "Santo Domingo De Los Tsachilas",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "934",
        "CodigoNegocio": "21",
        "Descripcion": "Sucumbios",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "919",
        "CodigoNegocio": "18",
        "Descripcion": "Tungurahua",
        "Nivel": "3",
        "CodigoPadre": "744"
      },
      {
        "IdDetalleCatalogo": "930",
        "CodigoNegocio": "19",
        "Descripcion": "Zamora Chinchipe",
        "Nivel": "3",
        "CodigoPadre": "744"
      }
    ];
    this.listaCantones = [
      {
        "IdDetalleCatalogo": "1023",
        "CodigoNegocio": "02",
        "Descripcion": "Cayambe",
        "Nivel": "4",
        "CodigoPadre": "924"
      },
      {
        "IdDetalleCatalogo": "1027",
        "CodigoNegocio": "03",
        "Descripcion": "Machachi",
        "Nivel": "4",
        "CodigoPadre": "924"
      },
      {
        "IdDetalleCatalogo": "3254",
        "CodigoNegocio": "08",
        "Descripcion": "Pedro Vicente Maldonado",
        "Nivel": "4",
        "CodigoPadre": "924"
      },
      {
        "IdDetalleCatalogo": "3255",
        "CodigoNegocio": "09",
        "Descripcion": "Puerto Quito",
        "Nivel": "4",
        "CodigoPadre": "924"
      },
      {
        "IdDetalleCatalogo": "1022",
        "CodigoNegocio": "01",
        "Descripcion": "Quito",
        "Nivel": "4",
        "CodigoPadre": "924"
      },
      {
        "IdDetalleCatalogo": "1026",
        "CodigoNegocio": "07",
        "Descripcion": "San Miguel De Los Bancos",
        "Nivel": "4",
        "CodigoPadre": "924"
      },
      {
        "IdDetalleCatalogo": "1024",
        "CodigoNegocio": "05",
        "Descripcion": "Sangolqui",
        "Nivel": "4",
        "CodigoPadre": "924"
      },
      {
        "IdDetalleCatalogo": "1028",
        "CodigoNegocio": "04",
        "Descripcion": "Tabacundo",
        "Nivel": "4",
        "CodigoPadre": "924"
      }
    ];
    this.listaParroquias = [
      {
        "IdDetalleCatalogo": "6925",
        "CodigoNegocio": "51",
        "Descripcion": "Aloag",
        "Nivel": "6",
        "CodigoPadre": "5076"
      },
      {
        "IdDetalleCatalogo": "6930",
        "CodigoNegocio": "52",
        "Descripcion": "Aloasi",
        "Nivel": "6",
        "CodigoPadre": "5076"
      },
      {
        "IdDetalleCatalogo": "6927",
        "CodigoNegocio": "53",
        "Descripcion": "Cutuglahua",
        "Nivel": "6",
        "CodigoPadre": "5076"
      },
      {
        "IdDetalleCatalogo": "6926",
        "CodigoNegocio": "54",
        "Descripcion": "El Chaupi",
        "Nivel": "6",
        "CodigoPadre": "5076"
      },
      {
        "IdDetalleCatalogo": "6924",
        "CodigoNegocio": "50",
        "Descripcion": "Machachi",
        "Nivel": "6",
        "CodigoPadre": "5076"
      },
      {
        "IdDetalleCatalogo": "6931",
        "CodigoNegocio": "55",
        "Descripcion": "Manuel Cornejo Astorga (tandapi)",
        "Nivel": "6",
        "CodigoPadre": "5076"
      },
      {
        "IdDetalleCatalogo": "6929",
        "CodigoNegocio": "56",
        "Descripcion": "Tambillo",
        "Nivel": "6",
        "CodigoPadre": "5076"
      },
      {
        "IdDetalleCatalogo": "6928",
        "CodigoNegocio": "57",
        "Descripcion": "Uyumbicho",
        "Nivel": "6",
        "CodigoPadre": "5076"
      }
    ];
  }

  formInicializar() {
    this.formularioDireccion = this._formBuilder.group({
      provincia: new FormControl(null, [
        Validators.required,
      ]),
      canton: new FormControl(null, [
        Validators.required,
      ]),
      parroquia: new FormControl(null, [
        Validators.required,
      ]),
      callePrincipal: new FormControl('XXXXXXXXXXX XXXXX', [
        Validators.required,
      ]),
      calleSecundaria: new FormControl('XXXXXXX XXXXXX', [
        Validators.required,
      ]),
      numeracion: new FormControl('XXX-XXXX', [
        Validators.required,
      ])
    })
  }

  get provinciaField() {
    this.devolverFormularioDireccion();
    // this.obtenerLocalidades(this.formularioDireccion.value.provincia.IdDetalleCatalogo, '4');
    return this.formularioDireccion.get('provincia') as FormControl;
  }

  get cantonField() {
    this.devolverFormularioDireccion();
    // this.obtenerLocalidades(this.formularioDireccion.value.canton.IdDetalleCatalogo, '5');
    return this.formularioDireccion.get('canton') as FormControl;
  }

  get parroquiaField() {
    this.devolverFormularioDireccion();
    return this.formularioDireccion.get('parroquia') as FormControl;
  }

  get callePrincipalField() {
    this.devolverFormularioDireccion();
    return this.formularioDireccion.get('callePrincipal') as FormControl;
  }

  get calleSecundariaField() {
    this.devolverFormularioDireccion();
    return this.formularioDireccion.get('calleSecundaria') as FormControl;
  }

  get numeracionField() {
    this.devolverFormularioDireccion();
    return this.formularioDireccion.get('numeracion') as FormControl;
  }

  devolverFormularioDireccion() {
    if (this.formularioDireccion.valid) {
      this.formDireccion.emit(this.formularioDireccion.value);
      this._storageService.setAddForm(false, 'direccion');
    } else {
      this._storageService.setAddForm(true, 'direccion');
    }
  }

  cambio(event: any, tipo: string) {
    switch (tipo) {
      case 'provincia':
        this.obtenerLocalidades(event.detail.value.IdDetalleCatalogo, '4');
        break;
      case 'canton':
        this.obtenerLocalidades(event.detail.value.IdDetalleCatalogo, '5');
        break;
    }
  }
}
