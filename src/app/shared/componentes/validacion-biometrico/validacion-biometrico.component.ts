import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {ModalGeneralService} from "../../../core/modales/modal-general.service";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {COOKIE_INFO_SOCIO} from "../../../core/constantes/nombres-cookies.constantes";
import {CookiesService} from "../../../core/services/cookies/cookies.service";

@Component({
  selector: 'app-validacion-biometrico',
  templateUrl: './validacion-biometrico.component.html',
  styleUrls: ['./validacion-biometrico.component.scss'],
})
export class ValidacionBiometricoComponent implements OnInit {

  @ViewChild('myIframe', {static: false}) myIframe: ElementRef | any;
  path = '';
  url: SafeResourceUrl | any;
  uri = '';

  respuestaBiometrico: any;

  informacionQuery: any;
  infoSocio: any;

  constructor(
    private readonly _router: Router,
    private readonly _domSanitizationService: DomSanitizer,
    private readonly _modalGeneralService: ModalGeneralService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _cookiesService: CookiesService,
  ) {
    this.path = this._router.url.split('/')[2];
    this._activatedRoute
      .queryParams
      .subscribe((params: any) => {
        this.informacionQuery = this._encriptadoService.desencriptarInformacionRutas(params.d);
      });
    const socioEncriptado = this._cookiesService.obtenerCookie(COOKIE_INFO_SOCIO);
    this.infoSocio = JSON.parse(this._encriptadoService.desencriptarInformacionCookie(socioEncriptado));
  }

  ngOnInit() {
    this.obtenerBiometrico();
  }

  ngAfterViewInit() {
    console.log('ejecuta posterior')
    const handleMessage = (event: { source: any; data: { status: any; details: { message: any; result: any }; }; }) => {
      if (event.source === this.myIframe.nativeElement.contentWindow) {
        console.log(event.data, '==>> data <<===')
        switch (event.data.status) {
          case 'completed':
            console.log('Proceso completado con éxito!');
            console.log('Resultado:', event.data.details.message);
            window.removeEventListener('message', handleMessage);
            this.continuar();
            break;
          case 'invalid url':
            console.log(event.data.details.message);
            // this.router.navigate(['/path'])
            break;
          case 'no match':
            console.log(event.data.details.message);
            // this.router.navigate(['/path'])
            break;
          case 'liveness failed':
            console.log(event.data.details.message);
            // this.router.navigate(['/path'])
            break;
          case 'idscan failed':
            console.log(event.data.details.message);
            // this.router.navigate(['/path'])
            break;
          case 'failed':
            console.log(event.data.details.message);
            // this.router.navigate(['/path'])
            break;
          default:
            console.log(event.data.details.message);
            if (event.data.details.message.includes('se completo el escaneo facial')) {
              this.respuestaBiometrico = event.data.details.result;
            }
            console.log('<<<==== Valor por default ====>>>')
            // this.router.navigate(['/path'])
            break;
        }
      }
    };

    window.addEventListener('message', handleMessage);
  }

  obtenerBiometrico(): void {
  }

  continuar(): void {
    let pathRe = `./home/${this.path}/informacion-personal`;

    const queryJSON = this._encriptadoService.encriptarInformacionRutas(JSON.stringify(this.informacionQuery));
    this._router
      .navigate([pathRe], {
        queryParams: {
          d: queryJSON
        },
        // relativeTo: this._activatedRoute,
        queryParamsHandling: 'merge'
      })
      .then();
  }
}
