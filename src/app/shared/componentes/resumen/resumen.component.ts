import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {TIPOS_CAPITALIZACION} from "../../../core/constantes/valores-constantes";
import {TiposCapitalizacion} from "../../../core/interfaces/lista-interfaces.interface";

@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.scss'],
})
export class ResumenComponent implements OnInit {

  path = '';
  informacionQuery: any = {};

  tiposCapitalizacion: TiposCapitalizacion[] = TIPOS_CAPITALIZACION;

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _encriptadoService: EncriptadoService
  ) {
    this.path = this._router.url.split('/')[2];

    this.informacionQuery = {
      "simulacion": {
        "monto": "80",
        "plazo": 12,
        "fechaInicio": "2023-06-27",
        "montoInicial": "25",
        "fechaExpiracion": "2024-06-03",
        "diaDebito": "15",
        "montoFinal": 999.91,
        "interesEfectivo": "7.50",
        "periodicidadTexto": "Vencimiento"
      },
      "logueado": true,
      "cuentaSeleccionada": {
        "cueDescripcion": "AHORROS SOCIO",
        "cueNumeroCuenta": "401011016632",
        "cueSaldoTotal": 1000,
        "cueSaldoDisponible": 995,
        "cueSaldoBloqueado": 0,
        "cueDescripcionEstado": "ACTIVA",
        "cueIdentificacionCliente": "1720446218",
        "promedioSemestral": 0,
        "idCuenta": "900756",
        "cueNumeroCuentaEnmascarada": "40XXXXXXXX32"
      }
    }

    this.informacionQuery.simulacion.periodicidadTexto = this.tiposCapitalizacion.find(
      (reg) => reg.periodicidad == this.informacionQuery?.simulacion?.periodicidad
    )?.nombre ?? 'Vencimiento';

    console.log(this.informacionQuery, '==>> query en resumen <<===');
  }

  ngOnInit() {
  }

  nuevaSimulacion(): void {
    // simulador
    const pathNS = `./home/${this.path}/simulador`
    this._router
      .navigate([pathNS])
      .then();
  }

  continuar(): void {
    const pathRe = `./home/${this.path}/instruccion-biometrico`
    this._router
      .navigate([pathRe])
      .then();
  }
}
