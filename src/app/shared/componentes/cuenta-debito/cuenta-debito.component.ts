import {Component, OnInit} from '@angular/core';
import {CuentasSocioInterface,} from "../../../core/interfaces/lista-interfaces.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {CookiesService} from "../../../core/services/cookies/cookies.service";

@Component({
  selector: 'app-cuenta-debito',
  templateUrl: './cuenta-debito.component.html',
  styleUrls: ['./cuenta-debito.component.scss'],
})
export class CuentaDebitoComponent implements OnInit {

  path = '';
  cuentasSocio = [
    {
      "cueDescripcion": "AHORROS SOCIO",
      "cueNumeroCuenta": "401011016632",
      "cueSaldoTotal": 1000,
      "cueSaldoDisponible": 995,
      "cueSaldoBloqueado": 0,
      "cueDescripcionEstado": "ACTIVA",
      "cueIdentificacionCliente": "1720446218",
      "promedioSemestral": 0,
      "idCuenta": "900756",
      "cueNumeroCuentaEnmascarada": "40XXXXXXXX32"
    }
  ]

  informacionQuery: any;
  infoSocio: any;

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _cookiesService: CookiesService,
  ) {
    this.path = this._router.url.split('/')[2];

    this._activatedRoute
      .queryParams
      .subscribe((params: any) => {
        this.informacionQuery = params.d;
      });

    this.infoSocio = {
      "esFuncionario":0,
      "esVinculado":0,
      "esCuentaNueva":0,
      "id":1,
      "tipoIdentificacion":"C",
      "identificacion":"17XXXXXXXX",
      "codigoDactilar":"VXXXXVXXXX",
      "nombresCompletos":"XXXXX XXXXXX XXXXXXX XXXXXX",
      "nombres":"JONATHAN JAVIER",
      "apellidoPaterno":" PARRA",
      "apellidoMaterno":"SOCASI",
      "email":"xxxxxxxx@xxxxxxxx.xxx",
      "telefono":"09XXXXXXXX",
      "fechaNacimiento":"2000-01-01T10:00:00.000Z",
      "nacionalidad":"XXXXXXXXXXX",
      "profesion":"ESTUDIANTE",
      "estudios":"SUPERIOR",
      "estadoCivil":"XXXXXXXX",
      "sexo":"HOMBRE",
      "cargas":0,
      "edad":27,
      "ingresoMensual":1335,
      "egresoMensual":0,
      "patrimonio":10000,
      "dependiente":"Dependiente",
      "tipoVivienda":"Familiar",
      "tipoSocio":"N",
      "region":"PICHINCHA",
      "cuotaEstimada":null,
      "cuotaAval":null,
      "montoAval":null,
      "plazoAval":null,
      "perfil":null,
      "nombreEmpresa":"COOPERATIVA DE AHORRO Y CREDITO 29 DE OCTUBRE LTDA.",
      "fechaIngresoEmpresa":"2021-09-06T10:00:00.000Z",
      "fechaRegistroBase":"2023-05-11T01:58:00.000Z",
      "direcciones":[
        {
          "id":1,
          "pais":"ECUADOR",
          "ciudad":"QUITO",
          "canton":"QUITO",
          "provincia":"PICHINCHA",
          "parroquia":"CONOCOTO",
          "callePrincipal":"ALDAZ 00 CENTRAL",
          "calleTransversal":"SN",
          "numeracion":"SN",
          "tipoDireccion":{
            "id":1,
            "descripcion":"DOMICILIARIA"
          }
        },
        {
          "id":2,
          "pais":"ECUADOR",
          "ciudad":"1701",
          "canton":"1701",
          "provincia":"17",
          "parroquia":"170156",
          "callePrincipal":"CAÑARIS OE6-140 AV. MARISCAL SUCRE DIAGONAL A LA PAPELERIA POPULAR",
          "calleTransversal":"SN",
          "numeracion":"SN",
          "tipoDireccion":{
            "id":2,
            "descripcion":"TRABAJO"
          }
        }
      ],
      "nombresIngresados":"JONATHAN JAVIER",
      "apellidosIngresados":"PARRA SOCASI",
      "login":64
    };

    console.log(this.informacionQuery, '==>> queryyy <<===');
    console.log(this.cuentasSocio, '===>> cuentas socio <<===')
  }

  ngOnInit() {
  }

  cuentaSeleccionada(cuenta: CuentasSocioInterface): void {
    const pathRe = `./home/${this.path}/resumen`;
    this._router
      .navigate([pathRe])
      .then();
  }

}
