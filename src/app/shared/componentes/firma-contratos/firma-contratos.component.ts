import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CargandoService} from "../../../core/services/cargando/cargando.service";
import {COOKIE_INFO_SOCIO} from "../../../core/constantes/nombres-cookies.constantes";
import {CookiesService} from "../../../core/services/cookies/cookies.service";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {ModalGeneralService} from "../../../core/modales/modal-general.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-firma-contratos',
  templateUrl: './firma-contratos.component.html',
  styleUrls: ['./firma-contratos.component.scss'],
})
export class FirmaContratosComponent implements OnInit {

  formularioContratos: FormGroup | any;

  path = '';

  informacionQuery: any;
  infoSocio = {
    "esFuncionario":0,
    "esVinculado":0,
    "esCuentaNueva":0,
    "id":1,
    "tipoIdentificacion":"C",
    "identificacion":"17XXXXXXXX",
    "codigoDactilar":"VXXXXVXXXX",
    "nombresCompletos":"XXXXX XXXXXX XXXXXXX XXXXXX",
    "nombres":"JONATHAN JAVIER",
    "apellidoPaterno":" PARRA",
    "apellidoMaterno":"SOCASI",
    "email":"xxxxxxxx@xxxxxxxx.xxx",
    "telefono":"09XXXXXXXX",
    "fechaNacimiento":"2000-01-01T10:00:00.000Z",
    "nacionalidad":"XXXXXXXXXXX",
    "profesion":"ESTUDIANTE",
    "estudios":"SUPERIOR",
    "estadoCivil":"XXXXXXXX",
    "sexo":"HOMBRE",
    "cargas":0,
    "edad":27,
    "ingresoMensual":1335,
    "egresoMensual":0,
    "patrimonio":10000,
    "dependiente":"Dependiente",
    "tipoVivienda":"Familiar",
    "tipoSocio":"N",
    "region":"PICHINCHA",
    "cuotaEstimada":null,
    "cuotaAval":null,
    "montoAval":null,
    "plazoAval":null,
    "perfil":null,
    "nombreEmpresa":"COOPERATIVA DE AHORRO Y CREDITO 29 DE OCTUBRE LTDA.",
    "fechaIngresoEmpresa":"2021-09-06T10:00:00.000Z",
    "fechaRegistroBase":"2023-05-11T01:58:00.000Z",
    "direcciones":[
      {
        "id":1,
        "pais":"ECUADOR",
        "ciudad":"QUITO",
        "canton":"QUITO",
        "provincia":"PICHINCHA",
        "parroquia":"CONOCOTO",
        "callePrincipal":"ALDAZ 00 CENTRAL",
        "calleTransversal":"SN",
        "numeracion":"SN",
        "tipoDireccion":{
          "id":1,
          "descripcion":"DOMICILIARIA"
        }
      },
      {
        "id":2,
        "pais":"ECUADOR",
        "ciudad":"1701",
        "canton":"1701",
        "provincia":"17",
        "parroquia":"170156",
        "callePrincipal":"CAÑARIS OE6-140 AV. MARISCAL SUCRE DIAGONAL A LA PAPELERIA POPULAR",
        "calleTransversal":"SN",
        "numeracion":"SN",
        "tipoDireccion":{
          "id":2,
          "descripcion":"TRABAJO"
        }
      }
    ],
    "nombresIngresados":"JONATHAN JAVIER",
    "apellidosIngresados":"PARRA SOCASI",
    "login":64
  };

  documentoCredito = '';
  documentoCuenta64 = '';
  documentoSeguroFamiliar = '';

  constructor(
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cookiesService: CookiesService,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _formBuilder: FormBuilder,
    private readonly _modalGeneralService: ModalGeneralService,
  ) {
    this.path = this._router.url.split('/')[2];
  }

  ngOnInit() {
    this.consultarDocumentosCredito();
    this.formInicializar();
  }

  formInicializar() {
    this.formularioContratos = this._formBuilder.group({
      credito: new FormControl({value: false, disabled: true}, [
        Validators.requiredTrue
      ]),
      cuenta: new FormControl({value: false, disabled: false}, [
        Validators.requiredTrue
      ]),
      biometrico: new FormControl({value: false, disabled: true}, [
        Validators.requiredTrue
      ]),
    })
  }

  get creditoField() {
    return this.formularioContratos.get('credito') as FormControl;
  }

  get cuentaField() {
    return this.formularioContratos.get('cuenta') as FormControl;
  }

  get biometricoField() {
    return this.formularioContratos.get('biometrico') as FormControl;
  }

  get seguroField() {
    return this.formularioContratos.get('seguro') as FormControl;
  }

  consultarDocumentosCredito(): void {
    this.documentoCuenta64 = '';
  }

  mostrarContratoPdf(): void {
    this._modalGeneralService
      .modalContratoPdf(
        'APERTURA DE CUENTA',
        this.documentoCuenta64
      ).then()
  }

  continuar(): void {
    const pathRe = `./home/${this.path}/resumen-informativo`;
    this._router
      .navigate([pathRe])
      .then()  }

}
