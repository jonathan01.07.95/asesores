import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {StorageService} from "../../../core/services/cargando/storage.service";

@Component({
  selector: 'app-componente-informacion-financiera',
  templateUrl: './componente-informacion-financiera.component.html',
  styleUrls: ['./componente-informacion-financiera.component.scss'],
})
export class ComponenteInformacionFinancieraComponent implements OnInit {

  formularioFinanciero: FormGroup | any;

  @Output() formFinanciero = new EventEmitter<any>()

  rangoMontos = [
    {
      rango: 'menor $400',
      valor: '400'
    },
    {
      rango: '$401 - $1000',
      valor: '1000'
    },
    {
      rango: '$1001 - $3000',
      valor: '3000'
    },
    {
      rango: '$3001 - $5000',
      valor: '5000'
    },
    {
      rango: '$5001 en adelante',
      valor: '10000'
    }
  ]

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _storageService: StorageService
  ) {
  }

  ngOnInit() {
    this.formInicializar();
  }

  formInicializar() {
    this.formularioFinanciero = this._formBuilder.group({
      ingresos: new FormControl(null, [
        Validators.required
      ]),
      egresos: new FormControl(null, [
        Validators.required
      ]),
      patrimonio: new FormControl(null, [
        Validators.required
      ])
    })
  }

  get ingresosField() {
    return this.formularioFinanciero.get('ingresos') as FormControl;
  }

  get egresosField() {
    return this.formularioFinanciero.get('egresos') as FormControl;
  }

  get patrimonioField() {
    return this.formularioFinanciero.get('patrimonio') as FormControl;
  }

  devolverFormularioFinanciero() {
    if (this.formularioFinanciero.valid) {
      this.formFinanciero.emit(this.formularioFinanciero.value);
      this._storageService.setAddForm(false, 'financiero');
    } else {
      this._storageService.setAddForm(true, 'financiero');
    }
  }


}
