import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ComponenteInformacionFinancieraComponent } from './componente-informacion-financiera.component';

describe('ComponenteInformacionFinancieraComponent', () => {
  let component: ComponenteInformacionFinancieraComponent;
  let fixture: ComponentFixture<ComponenteInformacionFinancieraComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponenteInformacionFinancieraComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ComponenteInformacionFinancieraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
