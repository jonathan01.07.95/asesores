import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {COOKIE_INFO_SOCIO} from "../../../core/constantes/nombres-cookies.constantes";
import {EncriptadoService} from "../../../core/services/encriptacion/encriptacion-aes.service";
import {CookiesService} from "../../../core/services/cookies/cookies.service";
import {enmascararFormulariosCorreo} from "../../../core/funciones/enmascarar";

@Component({
  selector: 'app-resumen-informativo',
  templateUrl: './resumen-informativo.component.html',
  styleUrls: ['./resumen-informativo.component.scss'],
})
export class ResumenInformativoComponent implements OnInit {
  mensajes = [
    {
      modulo: 'apertura-cuenta',
      mensaje: 'la cuenta ha sido CREADA EXITOSAMENTE.',
      path: 'assets/fondos/ahorro.png'
    },
    {
      modulo: 'ahorro-programado',
      mensaje: 'el ahorro programado, ha sido CREADO EXITOSAMENTE.',
      path: 'assets/fondos/ahorro.png'
    },
    {
      modulo: 'plazo-fijo',
      mensaje: 'el déposito a plazo fijo, ha sido CREADO EXITOSAMENTE.',
      path: 'assets/fondos/inversion.png'
    },
    {
      modulo: 'credito',
      mensaje: 'el desembolso del crédito, ha sido EXITOSAMENTE.',
      path: 'assets/fondos/credito.png'
    }
  ]
  mensajeDescripcion: any = {};

  public imagenFondo: string = 'assets/fondos/ahorro.png';

  path = '';
  informacionQuery: any;
  infoSocio: any;

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _encriptadoService: EncriptadoService,
    private readonly _cookiesService: CookiesService
  ) {
    this.path = this._router.url.split('/')[2];
    const modulo: any = this.mensajes.find((reg) => reg.modulo == this.path);
    this.mensajeDescripcion = modulo ?? {};

    this.infoSocio = {
      "esFuncionario":0,
      "esVinculado":0,
      "esCuentaNueva":0,
      "id":1,
      "tipoIdentificacion":"C",
      "identificacion":"17XXXXXXXX",
      "codigoDactilar":"VXXXXVXXXX",
      "nombresCompletos":"XXXXX XXXXXX XXXXXXX XXXXXX",
      "nombres":"JONATHAN JAVIER",
      "apellidoPaterno":" PARRA",
      "apellidoMaterno":"SOCASI",
      "email":"xxxxxxxx@xxxxxxxx.xxx",
      "telefono":"09XXXXXXXX",
      "fechaNacimiento":"2000-01-01T10:00:00.000Z",
      "nacionalidad":"XXXXXXXXXXX",
      "profesion":"ESTUDIANTE",
      "estudios":"SUPERIOR",
      "estadoCivil":"XXXXXXXX",
      "sexo":"HOMBRE",
      "cargas":0,
      "edad":27,
      "ingresoMensual":1335,
      "egresoMensual":0,
      "patrimonio":10000,
      "dependiente":"Dependiente",
      "tipoVivienda":"Familiar",
      "tipoSocio":"D",
      "region":"PICHINCHA",
      "cuotaEstimada":null,
      "cuotaAval":null,
      "montoAval":null,
      "plazoAval":null,
      "perfil":null,
      "nombreEmpresa":"COOPERATIVA DE AHORRO Y CREDITO 29 DE OCTUBRE LTDA.",
      "fechaIngresoEmpresa":"2021-09-06T10:00:00.000Z",
      "fechaRegistroBase":"2023-05-11T01:58:00.000Z",
      "direcciones":[
        {
          "id":1,
          "pais":"ECUADOR",
          "ciudad":"QUITO",
          "canton":"QUITO",
          "provincia":"PICHINCHA",
          "parroquia":"CONOCOTO",
          "callePrincipal":"ALDAZ 00 CENTRAL",
          "calleTransversal":"SN",
          "numeracion":"SN",
          "tipoDireccion":{
            "id":1,
            "descripcion":"DOMICILIARIA"
          }
        },
        {
          "id":2,
          "pais":"ECUADOR",
          "ciudad":"1701",
          "canton":"1701",
          "provincia":"17",
          "parroquia":"170156",
          "callePrincipal":"CAÑARIS OE6-140 AV. MARISCAL SUCRE DIAGONAL A LA PAPELERIA POPULAR",
          "calleTransversal":"SN",
          "numeracion":"SN",
          "tipoDireccion":{
            "id":2,
            "descripcion":"TRABAJO"
          }
        }
      ],
      "nombresIngresados":"JONATHAN JAVIER",
      "apellidosIngresados":"PARRA SOCASI",
      "login":64
    };
    this.infoSocio.email = enmascararFormulariosCorreo(this.infoSocio.email, 'X');

    console.log(this.informacionQuery, '===>>> queryy <<<====');
  }

  ngOnInit() {
  }

}
