import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from "@ionic/angular";
import {CamposModalGeneral} from "../../../core/interfaces/lista-interfaces.interface";

@Component({
  selector: 'app-modal-general',
  templateUrl: './modal-general.component.html',
  styleUrls: ['./modal-general.component.scss'],
})
export class ModalGeneralComponent implements OnInit {
  data: CamposModalGeneral = {titulo: '', cabecera: '', urlImagen: '', texto: '', botonAceptar: false };
  cargando = true;

  constructor(
    private readonly _modalController: ModalController,
    private readonly _navParams: NavParams
  ) {
    this.data = this._navParams.get('data');
    setTimeout(() => {
      this.cargando = !this.cargando;
    }, 1500);
  }

  ngOnInit() {
  }

  async cerrarModal() {
    await this._modalController.dismiss();
  }

  async aceptar() {
    await this._modalController.dismiss('confirmar');
  }

}
