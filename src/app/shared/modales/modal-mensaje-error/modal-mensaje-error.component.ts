import {Component, OnInit} from '@angular/core';
import {CamposModalGeneral} from "../../../core/interfaces/lista-interfaces.interface";
import {ModalController, NavParams} from "@ionic/angular";

@Component({
  selector: 'app-modal-mensaje-error',
  templateUrl: './modal-mensaje-error.component.html',
  styleUrls: ['./modal-mensaje-error.component.scss'],
})
export class ModalMensajeErrorComponent implements OnInit {
  data: CamposModalGeneral = {titulo: '', urlImagen: ''};
  cargando = true;

  constructor(
    private readonly _modalController: ModalController,
    private readonly _navParams: NavParams
  ) {
    this.data = this._navParams.get('data');
    setTimeout(() => {
      this.cargando = !this.cargando;
    }, 1500);
  }

  ngOnInit() {
  }

  async cerrarModal() {
    await this._modalController.dismiss();
  }

}
