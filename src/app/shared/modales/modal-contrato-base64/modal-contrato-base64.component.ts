import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from "@ionic/angular";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";

@Component({
  selector: 'app-modal-contrato-base64',
  templateUrl: './modal-contrato-base64.component.html',
  styleUrls: ['./modal-contrato-base64.component.scss'],
})
export class ModalContratoBase64Component implements OnInit {
  data: any;
  safePdfBase64String: SafeResourceUrl;

  constructor(
    private readonly _modalController: ModalController,
    private readonly _navParams: NavParams,
    private readonly sanitizer: DomSanitizer
  ) {
    this.data = this._navParams.get('data');
    this.safePdfBase64String =
      this.sanitizer.bypassSecurityTrustResourceUrl('');
  }

  ngOnInit() {}

  async loadPdf(event: Event) {
    /*const target = event.target as HTMLInputElement;
    const files = target.files as FileList;

    const file = files.item(0);

    if (!file) {
      return;
    }

    const rawPdfBase64String = await convertFileToBase64String(file);

    // 👇 use Angular's DomSanitizer to transform string value into SafeResourceUrl
    this.safePdfBase64String =
      this.sanitizer.bypassSecurityTrustResourceUrl(rawPdfBase64String);*/
  }

  async cerrarModal() {
    await this._modalController.dismiss();
  }


}
