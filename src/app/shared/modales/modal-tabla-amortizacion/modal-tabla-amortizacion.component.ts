import { Component, OnInit } from '@angular/core';
import {
  CamposModalGeneral,
  TablaAmortizacionInterface
} from "../../../core/interfaces/lista-interfaces.interface";
import {ModalController, NavParams} from "@ionic/angular";

@Component({
  selector: 'app-modal-tabla-amortizacion',
  templateUrl: './modal-tabla-amortizacion.component.html',
  styleUrls: ['./modal-tabla-amortizacion.component.scss'],
})
export class ModalTablaAmortizacionComponent implements OnInit {
  data: TablaAmortizacionInterface = {titulo: '', info: ''};

  constructor(
    private readonly _modalController: ModalController,
    private readonly _navParams: NavParams
  ) {
    this.data = this._navParams.get('data');
    console.log(this.data, '===>> data tabla <<<====')
  }

  ngOnInit() {}

  async cerrarModal() {
    await this._modalController.dismiss();
  }

}
