import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalTablaAmortizacionComponent } from './modal-tabla-amortizacion.component';

describe('ModalTablaAmortizacionComponent', () => {
  let component: ModalTablaAmortizacionComponent;
  let fixture: ComponentFixture<ModalTablaAmortizacionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTablaAmortizacionComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalTablaAmortizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
