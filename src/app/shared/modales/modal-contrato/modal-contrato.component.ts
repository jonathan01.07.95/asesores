import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from "@ionic/angular";
import {CamposModalGeneral} from "../../../core/interfaces/lista-interfaces.interface";

@Component({
  selector: 'app-modal-contrato',
  templateUrl: './modal-contrato.component.html',
  styleUrls: ['./modal-contrato.component.scss'],
})
export class ModalContratoComponent implements OnInit {
  data: CamposModalGeneral | any = {titulo: '', base64: ''};
  pdfSrc: string = '';

  constructor(
    private readonly _modalController: ModalController,
    private readonly _navParams: NavParams
  ) { }

  ngOnInit() {}

  ionViewDidEnter() {
    const file = new File([this.data.base64], 'file.pdf', { type: 'application/pdf' });
    this.pdfSrc = URL.createObjectURL(file);
    console.log(this.data, '===>> data <<<===', this.pdfSrc);
  }

  async cerrarModal() {
    await this._modalController.dismiss();
  }
}
