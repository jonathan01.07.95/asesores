import { CommonModule } from '@angular/common';
import {NgModule} from "@angular/core";
import {
  FormularioInformacionPersonalComponent
} from "./componentes/formulario-informacion-personal/formulario-informacion-personal.component";
import {IonicModule} from "@ionic/angular";
import {ModalGeneralComponent} from "./modales/modal-general/modal-general.component";
import {VerificacionOtpComponent} from "./componentes/verificacion-otp/verificacion-otp.component";
import {CodeInputModule} from "angular-code-input";
import {TipoCuentaComponent} from "./componentes/tipo-cuenta/tipo-cuenta.component";
import {FirmaContratosComponent} from "./componentes/firma-contratos/firma-contratos.component";
import {ValidacionBiometricoComponent} from "./componentes/validacion-biometrico/validacion-biometrico.component";
import {
  InstruccionesBiometricoComponent
} from "./componentes/instrucciones-biometrico/instrucciones-biometrico.component";
import {FormularioIngresoComponent} from "./componentes/formulario-ingreso/formulario-ingreso.component";
import {
  ComponenteInformacionPersonalComponent
} from "./componentes/componente-informacion-personal/componente-informacion-personal.component";
import {
  ComponenteInformacionDomiciliariaComponent
} from "./componentes/componente-informacion-domiciliaria/componente-informacion-domiciliaria.component";
import {
  ComponenteInformacionFinancieraComponent
} from "./componentes/componente-informacion-financiera/componente-informacion-financiera.component";
import {ResumenInformativoComponent} from "./componentes/resumen-informativo/resumen-informativo.component";
import {SimuladorAhorroComponent} from "./componentes/simulador-ahorro/simulador-ahorro.component";
import {SimuladorDepositoComponent} from "./componentes/simulador-deposito/simulador-deposito.component";
import {SimuladorCreditoComponent} from "./componentes/simulador-credito/simulador-credito.component";
import {ResumenComponent} from "./componentes/resumen/resumen.component";
import {CuentaDebitoComponent} from "./componentes/cuenta-debito/cuenta-debito.component";
import {ReactiveFormsModule} from "@angular/forms";
import {ModalContratoComponent} from "./modales/modal-contrato/modal-contrato.component";
import {ModalContratoBase64Component} from "./modales/modal-contrato-base64/modal-contrato-base64.component";
import {ModalMensajeErrorComponent} from "./modales/modal-mensaje-error/modal-mensaje-error.component";
import {
  ModalTablaAmortizacionComponent
} from "./modales/modal-tabla-amortizacion/modal-tabla-amortizacion.component";
import {CookiesService} from "../core/services/cookies/cookies.service";

@NgModule({
  declarations: [
    ModalGeneralComponent,
    ModalContratoComponent,
    ModalContratoBase64Component,
    ModalMensajeErrorComponent,
    FormularioInformacionPersonalComponent,
    VerificacionOtpComponent,
    TipoCuentaComponent,
    FirmaContratosComponent,
    ValidacionBiometricoComponent,
    InstruccionesBiometricoComponent,
    FormularioIngresoComponent,
    ComponenteInformacionPersonalComponent,
    ComponenteInformacionDomiciliariaComponent,
    ComponenteInformacionFinancieraComponent,
    ResumenInformativoComponent,
    SimuladorAhorroComponent,
    SimuladorDepositoComponent,
    SimuladorCreditoComponent,
    ResumenComponent,
    CuentaDebitoComponent,
    ModalTablaAmortizacionComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    CodeInputModule,
    ReactiveFormsModule,
  ],
  exports: [
    ModalGeneralComponent,
    ModalContratoComponent,
    ModalContratoBase64Component,
    ModalMensajeErrorComponent,
    FormularioInformacionPersonalComponent,
    VerificacionOtpComponent,
    TipoCuentaComponent,
    FirmaContratosComponent,
    ValidacionBiometricoComponent,
    InstruccionesBiometricoComponent,
    FormularioIngresoComponent,
    ComponenteInformacionPersonalComponent,
    ComponenteInformacionDomiciliariaComponent,
    ComponenteInformacionFinancieraComponent,
    ResumenInformativoComponent,
    SimuladorAhorroComponent,
    SimuladorDepositoComponent,
    SimuladorCreditoComponent,
    ResumenComponent,
    CuentaDebitoComponent,
    ModalTablaAmortizacionComponent
  ],
  providers: [
    CookiesService,
  ]
})
export class SharedModule {}
