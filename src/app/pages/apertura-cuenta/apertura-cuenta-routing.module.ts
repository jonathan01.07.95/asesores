import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AperturaCuentaPage } from './apertura-cuenta.page';
import {VerificacionOtpComponent} from "../../shared/componentes/verificacion-otp/verificacion-otp.component";
import {FormularioIngresoComponent} from "../../shared/componentes/formulario-ingreso/formulario-ingreso.component";
import {TipoCuentaComponent} from "../../shared/componentes/tipo-cuenta/tipo-cuenta.component";
import {
  InstruccionesBiometricoComponent
} from "../../shared/componentes/instrucciones-biometrico/instrucciones-biometrico.component";
import {
  ValidacionBiometricoComponent
} from "../../shared/componentes/validacion-biometrico/validacion-biometrico.component";
import {
  FormularioInformacionPersonalComponent
} from "../../shared/componentes/formulario-informacion-personal/formulario-informacion-personal.component";
import {FirmaContratosComponent} from "../../shared/componentes/firma-contratos/firma-contratos.component";
import {ResumenInformativoComponent} from "../../shared/componentes/resumen-informativo/resumen-informativo.component";
const routes: Routes = [
  {
    path: '',
    component: AperturaCuentaPage,
    children: [
      {
        path: 'ingreso',
        component: FormularioIngresoComponent
      },
      {
        path: 'verificacion-otp',
        component: VerificacionOtpComponent
      },
      {
        path: 'tipo-cuenta',
        component: TipoCuentaComponent
      },
      {
        path: 'instruccion-biometrico',
        component: InstruccionesBiometricoComponent
      },
      {
        path: 'validacion-biometrica',
        component: ValidacionBiometricoComponent
      },
      {
        path: 'instruccion-biometrico',
        component: InstruccionesBiometricoComponent
      },
      {
        path: 'informacion-personal',
        component: FormularioInformacionPersonalComponent
      },
      {
        path: 'firma-contratos',
        component: FirmaContratosComponent
      },
      {
        path: 'resumen-informativo',
        component: ResumenInformativoComponent
      },
      {
        path: '',
        redirectTo: 'ingreso',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AperturaCuentaPageRoutingModule {}
