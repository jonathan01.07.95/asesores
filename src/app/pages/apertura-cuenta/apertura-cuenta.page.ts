import {Component, OnInit} from '@angular/core';
import {CookiesService} from "../../core/services/cookies/cookies.service";
import {StorageService} from "../../core/services/cargando/storage.service";

@Component({
  selector: 'app-apertura-cuenta',
  templateUrl: './apertura-cuenta.page.html',
  styleUrls: ['./apertura-cuenta.page.scss'],
})
export class AperturaCuentaPage implements OnInit {
  logueado?: any;

  constructor(
    private readonly _cookiesService: CookiesService,
    private readonly _storageService: StorageService
  ) {
    this._storageService.AddLoagueado$
      .subscribe((login) => {
        this.logueado = login;
      });
  }

  ngOnInit() {
  }

  cerrar(): void {
    this._cookiesService.eliminarCookieCierreFlujo();
    this._storageService.setAddLogueado(undefined);
  }
}
