import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AperturaCuentaPageRoutingModule } from './apertura-cuenta-routing.module';
import { AperturaCuentaPage } from './apertura-cuenta.page';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  declarations: [AperturaCuentaPage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AperturaCuentaPageRoutingModule,
    SharedModule
  ],
})
export class AperturaCuentaPageModule {}
