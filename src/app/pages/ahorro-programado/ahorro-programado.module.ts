import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AhorroProgramadoPageRoutingModule } from './ahorro-programado-routing.module';

import { AhorroProgramadoPage } from './ahorro-programado.page';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AhorroProgramadoPageRoutingModule,
    SharedModule
  ],
  declarations: [AhorroProgramadoPage]
})
export class AhorroProgramadoPageModule {}
