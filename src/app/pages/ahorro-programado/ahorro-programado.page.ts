import { Component, OnInit } from '@angular/core';
import {CookiesService} from "../../core/services/cookies/cookies.service";
import {StorageService} from "../../core/services/cargando/storage.service";

@Component({
  selector: 'app-ahorro-programado',
  templateUrl: './ahorro-programado.page.html',
  styleUrls: ['./ahorro-programado.page.scss'],
})
export class AhorroProgramadoPage implements OnInit {
  logueado: any;

  constructor(
    private readonly _cookiesService: CookiesService,
    private readonly _storageService: StorageService
  ) {
    this._storageService.AddLoagueado$.subscribe((login) => {
      this.logueado = login;
    });
  }

  ngOnInit() {
  }

  cerrar(): void {
    console.log('====>>> SALIR <<<====');
    this._cookiesService.eliminarCookieCierreFlujo();
    this._storageService.setAddLogueado(undefined);
  }

}
