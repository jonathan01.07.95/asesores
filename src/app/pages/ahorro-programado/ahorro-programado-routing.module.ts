import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AhorroProgramadoPage } from './ahorro-programado.page';
import {FormularioIngresoComponent} from "../../shared/componentes/formulario-ingreso/formulario-ingreso.component";
import {VerificacionOtpComponent} from "../../shared/componentes/verificacion-otp/verificacion-otp.component";
import {TipoCuentaComponent} from "../../shared/componentes/tipo-cuenta/tipo-cuenta.component";
import {
  InstruccionesBiometricoComponent
} from "../../shared/componentes/instrucciones-biometrico/instrucciones-biometrico.component";
import {
  ValidacionBiometricoComponent
} from "../../shared/componentes/validacion-biometrico/validacion-biometrico.component";
import {
  FormularioInformacionPersonalComponent
} from "../../shared/componentes/formulario-informacion-personal/formulario-informacion-personal.component";
import {FirmaContratosComponent} from "../../shared/componentes/firma-contratos/firma-contratos.component";
import {ResumenInformativoComponent} from "../../shared/componentes/resumen-informativo/resumen-informativo.component";
import {SimuladorAhorroComponent} from "../../shared/componentes/simulador-ahorro/simulador-ahorro.component";
import {ResumenComponent} from "../../shared/componentes/resumen/resumen.component";
import {CuentaDebitoComponent} from "../../shared/componentes/cuenta-debito/cuenta-debito.component";

const routes: Routes = [
  {
    path: '',
    component: AhorroProgramadoPage,
    children: [
      {
        path: 'simulador',
        component: SimuladorAhorroComponent
      },
      {
        path: 'ingreso',
        component: FormularioIngresoComponent
      },
      {
        path: 'verificacion-otp',
        component: VerificacionOtpComponent
      },
      {
        path: 'seleccion-cuenta-debito',
        component: CuentaDebitoComponent
      },
      {
        path: 'resumen',
        component: ResumenComponent
      },
      {
        path: 'instruccion-biometrico',
        component: InstruccionesBiometricoComponent
      },
      {
        path: 'validacion-biometrica',
        component: ValidacionBiometricoComponent
      },
      {
        path: 'informacion-personal',
        component: FormularioInformacionPersonalComponent
      },
      {
        path: 'resumen-informativo',
        component: ResumenInformativoComponent
      },
      {
        path: '',
        redirectTo: 'simulador',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AhorroProgramadoPageRoutingModule {}
