import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {NavController} from "@ionic/angular";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginDisplay = false;

  constructor(
    private readonly _router: Router,
    private readonly _navController: NavController,
  ) {
  }

  ngOnInit(): void {}

  ingresar(): void {
    this._navController.navigateForward('/home/apertura-cuenta').then();
  }
}
