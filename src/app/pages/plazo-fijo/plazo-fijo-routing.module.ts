import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlazoFijoPage } from './plazo-fijo.page';
import {SimuladorAhorroComponent} from "../../shared/componentes/simulador-ahorro/simulador-ahorro.component";
import {FormularioIngresoComponent} from "../../shared/componentes/formulario-ingreso/formulario-ingreso.component";
import {VerificacionOtpComponent} from "../../shared/componentes/verificacion-otp/verificacion-otp.component";
import {CuentaDebitoComponent} from "../../shared/componentes/cuenta-debito/cuenta-debito.component";
import {ResumenComponent} from "../../shared/componentes/resumen/resumen.component";
import {
  InstruccionesBiometricoComponent
} from "../../shared/componentes/instrucciones-biometrico/instrucciones-biometrico.component";
import {
  ValidacionBiometricoComponent
} from "../../shared/componentes/validacion-biometrico/validacion-biometrico.component";
import {FirmaContratosComponent} from "../../shared/componentes/firma-contratos/firma-contratos.component";
import {ResumenInformativoComponent} from "../../shared/componentes/resumen-informativo/resumen-informativo.component";
import {SimuladorDepositoComponent} from "../../shared/componentes/simulador-deposito/simulador-deposito.component";
import {
  FormularioInformacionPersonalComponent
} from "../../shared/componentes/formulario-informacion-personal/formulario-informacion-personal.component";

const routes: Routes = [
  {
    path: '',
    component: PlazoFijoPage,
    children: [
      {
        path: 'simulador',
        component: SimuladorDepositoComponent
      },
      {
        path: 'ingreso',
        component: FormularioIngresoComponent
      },
      {
        path: 'verificacion-otp',
        component: VerificacionOtpComponent
      },
      {
        path: 'seleccion-cuenta-debito',
        component: CuentaDebitoComponent
      },
      {
        path: 'resumen',
        component: ResumenComponent
      },
      {
        path: 'instruccion-biometrico',
        component: InstruccionesBiometricoComponent
      },
      {
        path: 'validacion-biometrica',
        component: ValidacionBiometricoComponent
      },
      {
        path: 'informacion-personal',
        component: FormularioInformacionPersonalComponent
      },
      {
        path: 'resumen-informativo',
        component: ResumenInformativoComponent
      },
      {
        path: '',
        redirectTo: 'simulador',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlazoFijoPageRoutingModule {}
