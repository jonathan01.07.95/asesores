import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlazoFijoPageRoutingModule } from './plazo-fijo-routing.module';
import { PlazoFijoPage } from './plazo-fijo.page';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlazoFijoPageRoutingModule,
    SharedModule
  ],
  declarations: [PlazoFijoPage]
})
export class PlazoFijoPageModule {}
