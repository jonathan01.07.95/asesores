import {Component, OnInit} from '@angular/core';
import {CookiesService} from "../../core/services/cookies/cookies.service";
import {StorageService} from "../../core/services/cargando/storage.service";

@Component({
  selector: 'app-plazo-fijo',
  templateUrl: './plazo-fijo.page.html',
  styleUrls: ['./plazo-fijo.page.scss'],
})
export class PlazoFijoPage implements OnInit {
  logueado: any;

  constructor(
    private readonly _cookiesService: CookiesService,
    private readonly _storageService: StorageService
  ) {
    this._storageService.AddLoagueado$.subscribe((login) => {
      this.logueado = login;
    });
  }

  ngOnInit() {
  }

  cerrar() {
    console.log('====>>> SALIR <<<====');
    this._cookiesService.eliminarCookieCierreFlujo();
    this._storageService.setAddLogueado(undefined);
  }
}
