import { MsalGuardConfiguration } from '@azure/msal-angular';
import { InteractionType, IPublicClientApplication, PublicClientApplication } from '@azure/msal-browser';
import { environment } from '../../environments/environment';

export class Msal {
  //* funcion para el guard con ad de azure
  static guardConfigFactory(): MsalGuardConfiguration {
    return {
      interactionType: InteractionType.Popup,
      authRequest: {
        scopes: ['user.read'],
      },
      loginFailedRoute: '/',
    };
  }

  static instanceFactory(): IPublicClientApplication {
    return new PublicClientApplication({
      auth: {
        clientId: environment.clientId,
        redirectUri: environment.redirectUri,
        authority: `${environment.urlMicrosoft}${environment.tenantId}`, // 'https://login.microsoftonline.com/' + 'f8733e19-5615-4258-907e-cb8c982ffc34',
      },
    });
  }
}
