// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  host: 'https://localhost:3030',
  pathApi: 'api-comercial',
  permisosAzure: ['user.read'],
  urlInformacionUsuarioAzure: 'https://graph.microsoft.com/v1.0/me',
  clientId: 'a350487d-f938-424d-9a55-09f310091f70',
  tenantId: 'f8733e19-5615-4258-907e-cb8c982ffc34',
  urlMicrosoft: 'https://login.microsoftonline.com/',
  redirectUri: '/home/apertura-cuenta/ingreso',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
